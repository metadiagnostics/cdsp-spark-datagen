# This file contains the drill sql queries to make the LPI patient data tables
# The ‘backup’ data files are located in 1) /CIS/cis_data or 2) /lpi/synth_data_rand_v1/
# @Author WBlack

#Medications table
Create table dfs.cis.`Medications` as SELECT enc.PatientID as patientID,
med.EncounterID as encounterID,
med.MedOrderID as medicationOrderID,
med.MedicationName as medicationName,
med.MedicationDose as medicationDose,
med.MedicationDoseUnits as medicationDoseUnits,
med.MedicationQuantity as medicationQuantity,
med.MedicationID as medicationNameID,
med.MedOrderDate as medicationOrderDate,
med.MedOrderCompleteDate as medicationOrderCompleteDate

FROM (SELECT * FROM dfs.`/lpi/synth_data_rand_v1/medications.txt.gz` ) as med LEFT JOIN  (SELECT * FROM dfs.`/lpi/synth_data_rand_v1/encounters.txt.gz`) as enc
ON med.EncounterID = enc.EncounterID;

#Encounters table
Create table dfs.cis.`Encounters` as select PatientID as patientID,
EncounterID as encounterID,
AdmitDate as admitDate,
DischargeDate as dischargeDate,
EncounterType as encounterType,
FacilityID as facilityID

FROM dfs.`/lpi/synth_data_rand_v1/encounters.txt.gz`;

# Laboratory table
Create table dfs.cis.`Laboratory` as select enc.PatientID as patientID,
lab.EncounterID as encounterID,
lab.LabOrderID as laboratoryOrderID,
lab.LabName as labName, 
lab.LabDescription as labDescription,
lab.LabValue as labResultsNum,
lab.LabValueUnits as labResultsUnits,
lab.AccessionID as labaccessionID,
lab.LabOrderDate as labOrderDate,
lab.LabCompletedDate as labCompleteDate

FROM (SELECT * FROM dfs.`/lpi/synth_data_rand_v1/labs.txt.gz`) as lab LEFT JOIN  
	(SELECT * FROM dfs.`/lpi/synth_data_rand_v1/encounters.txt.gz`) as enc
ON lab.EncounterID = enc.EncounterID;

#Procedures table
Create table dfs.cis.`Procedures` as select enc.PatientID as patientID,
proc.EncounterID as encounterID,
proc.ProcedureOrderID as procedureOrderID,
proc.ProcedureName as procedureName,
proc.ProcedureDate as procedureDate

FROM (SELECT * FROM dfs.`/lpi/synth_data_rand_v1/procedures.txt.gz`) as proc LEFT JOIN (SELECT * FROM dfs.`/lpi/synth_data_rand_v1/encounters.txt.gz`) as enc
ON proc.EncounterID = enc.EncounterID;

#Notes table
Create table dfs.cis.`Notes` as select enc.PatientID as patientID,
note.EncounterID as encounterID,
note.NoteID as noteID,
note.AuthorID as authorID,
note.NoteDate as noteDate
FROM (SELECT * FROM dfs.`/lpi/synth_data_rand_v1/notes.txt.gz`) as note LEFT JOIN 
(SELECT * FROM dfs.`/lpi/synth_data_rand_v1/encounters.txt.gz`) as enc
ON note.EncounterID = enc.EncounterID

#Facilities table
Create table dfs.cis.`Facilities` as select enc.EncounterID as patientID,
fac.FacilityID as facilityID,
fac.FacilityName as facilityName,
fac.FacilityLocation as facilityLocation

FROM (SELECT * FROM dfs.`/lpi/synth_data_rand_v1/facilities.txt.gz`) as fac, 
(SELECT * FROM dfs.`/lpi/synth_data_rand_v1/encounters.txt.gz`) as enc
WHERE fac.FacilityID = enc.FacilityID

#Patients table
Create table dfs.cis.`Patients` (patientID, firstName, lastName, gender, dob) 
 as select pt.PatientID as patientID,
pt.FirstName as firstName,
pt.LastName as lastName,
pt.Gender as gender,
pt.DOB as dob
FROM dfs.`/lpi/synth_data_rand_v1/patients.txt.gz` pt ;

# Jeff requested that region be added to patient table
Drop table dfs.cis.Patients;
Create table dfs.cis.`Patients` (patientID, firstName, lastName, gender, dob, region) 
 as select pt.PatientID as patientID,
pt.FirstName as firstName,
pt.LastName as lastName,
pt.Gender as gender,
pt.DOB as dob,
pa.region as region
FROM 
(SELECT * FROM dfs.`/lpi/synth_data_rand_v1/patients.txt.gz`) pt LEFT JOIN (SELECT * FROM dfs.cis.patientAddress) pa 
ON pt.PatientID = pa.patientID ;

#Diagnosis Table
Create table dfs.cis.`Diagnoses` as select enc.PatientID as patientID,
dx.EncounterID as encounterID,
dx.DiagnosisId as diagnosisID,
dx.DiagnosisCode as diagnosisCode,
dx.DiagnosisDescription as diagnosisDescription

FROM (SELECT * FROM dfs.`/lpi/synth_data_rand_v1/diagnoses.txt.gz`) as dx LEFT JOIN 
(SELECT * FROM dfs.`/lpi/synth_data_rand_v1/encounters.txt.gz`) as enc
ON dx.EncounterID = enc.EncounterID ;


#PCI Feature Table for 1 million patients
Create table dfs.cis.PCI_Feature_Data as select * from dfs.cis.`/cis_data/PCI_Feature_Data.txt`

#PCI Feature table for 3 million patients
Create table dfs.cis.PCI_Feature_Data as select * from dfs.cis.`/cis_data/PCI_Feature_Data3million.txt`

#Suicide Feature Table for 1 million patients
Create table dfs.cis.`Suicide_Feature_Data` as select * FROM dfs.cis.Suicide_Feature_Data.txt;

#Suicide Feature Table for 3 million patients
Create table dfs.cis.Suicide_Feature_Data as select * FROM dfs.cis.`/cis_data/Suicide_Feature_Data3million.txt`;

#Military Service table
Create table dfs.cis.`MilitaryService` as select * FROM dfs.`/CIS/Military.txt`;

#Patient Address table
Create table dfs.cis.patientAddress as select * from dfs.cis.`/cis_data/patientAddress.txt`

