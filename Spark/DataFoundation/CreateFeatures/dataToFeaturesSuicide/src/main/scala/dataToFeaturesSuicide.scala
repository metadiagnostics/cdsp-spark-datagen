/*
 *
 *  * Copyright 2016 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *  *
 *  * Licensed under the Apache License, Version 2.0 (the "License");
 *  * you may not use this file except in compliance with the License.
 *  * You may obtain a copy of the License at
 *  *
 *  * http://www.apache.org/licenses/LICENSE-2.0
 *  *
 *  * Unless required by applicable law or agreed to in writing, software
 *  * distributed under the License is distributed on an "AS IS" BASIS,
 *  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  * See the License for the specific language governing permissions and
 *  * limitations under the License.
 *
 *
 */
import org.apache.hadoop.security.UserGroupInformation
import org.apache.spark.{SparkConf, SparkContext}
import org.apache.spark.SparkContext._
import org.joda.time.{DateTime, Days}

import scala.io.Source
import org.apache.spark.sql._
import org.apache.spark.sql.types.{MapType, StringType, StructField, StructType}
import org.apache.spark.sql.hive
/**
  * The purpose of this application is to transform electronic medical record data into features (parameters) for a
  * previously discovered or defined statistical model.
  * The specific statistical model is for suicide risk. (This model is ficticious and in no way should it be used to
  * predict, classify, or guide decisions on suicide risk, treatment, or interventions etc)
  * The EMR data is assumed to consist of medications orders, laboratory test orders and results, procedure orders.
  * The features of the suicide risk model are:
  * 1. male gender, 2. enlistment age >= 27 years, 3. prior suicide attempt, 4. number of outpatient encounters with
  * suicide ideation in past 12 months, 5. number of outpatient encounters with a mental health professional in last
  * 12 months, 6. number of antidepressants prescriptions filled in last 12 months, 7. number of psychiatric
  * hospitalizations/time in service >50th percentile, 8. any prior inpat psychiatric treatment in past 12 months,
  * 9. number of inpt days in past 12 months for major depression,
  * 10. somatoform or disassociative disorder, 11. diagnosis of ptsd or depression, 12. substance misuse (alcohol)
  * Created by wblack on 5/3/16.
  *
  * NOTE: Replace the <PROJDIR> below with your Project Directory
  *
  */
object dataToFeaturesSuicide {
  val conf = new SparkConf().setAppName("dataToFeaturesSuicide").setMaster("local")
  val sc = new SparkContext(conf)

  def createListFromFile(resoursepath : String) : List[String] ={
    val outList = Source.fromInputStream(getClass.getResourceAsStream(resoursepath)).getLines().toList
    outList
  }

  def main(args: Array[String]) {
    val sqlContext = new org.apache.spark.sql.SQLContext(sc) //SQL Context
    import sqlContext.implicits._

    val hiveContext = new org.apache.spark.sql.hive.HiveContext(sc) //Hive Context
    import  hiveContext.implicits._
    //Schemas
    val schemaNamesPt = "patientID firstName lastName gender ptdob"
    val schemaNamesMeds = "encounterID medOrderID medName medDose medDoseUnits medQuantity medID medOrderDate medOrderComplete"
    val schemaNamesProcs = "encounterID procOrderID procName procDate"
    val schemaNamesDx = "encounterID diagnosisID diagnosisCode diagnosisDescription"
    val schemaNamesEncs = "patientID encounterID admitDate dischargeDate encounterType facilityID"
    val schemaNamesMil = "patientID service rank promotionDate"

    // Patient data, read the data into an RDD from text file, create schema, create dataframe, register table alias
    val patientData = sc.textFile("<PROJDIR>/Junk_Data_Generator/Data_Generated/patients.txt").map(_.split("\t")).map(p => Row(p(0), p(1).trim, p(2), p(3), p(4)))
    val schemaPt = StructType(schemaNamesPt.split(" ").map(fieldName => StructField(fieldName, StringType, true)))
    val patientDataSchemaRDD = hiveContext.createDataFrame(patientData, schemaPt)
    patientDataSchemaRDD.registerTempTable("patients")

    //Medications
    val medicationData = sc.textFile("/lpi/synth_data_rand_v1/medications.txt.gz").map(_.split("\t")).map(p => Row(p(0), p(1), p(2), p(3), p(4), p(5), p(6), p(7), p(8)))
    val schemaMeds = StructType(schemaNamesMeds.split(" ").map(fieldName => StructField(fieldName, StringType, true)))
    val medsDataSchemaRDD = hiveContext.createDataFrame(medicationData, schemaMeds)
    medsDataSchemaRDD.registerTempTable("meds")

    //Procedures
    val procData = sc.textFile("/lpi/synth_data_rand_v1/procedures.txt.gz").map(_.split("\t")).map(p => Row(p(0), p(1), p(2), p(3)))
    val schemaProc = StructType(schemaNamesProcs.split(" ").map(fieldName => StructField(fieldName, StringType, true)))
    val procsDataSchemaRDD = hiveContext.createDataFrame(procData, schemaProc)
    procsDataSchemaRDD.registerTempTable("procs")

    //Diagnosis
    val dxData = sc.textFile("/lpi/synth_data_rand_v1/diagnoses.txt.gz").map(_.split("\t")).map(p => Row(p(0), p(1), p(2), p(3)))
    val schemaDx = StructType(schemaNamesDx.split(" ").map(fieldName => StructField(fieldName, StringType, true)))
    val dxDataSchemaRDD = hiveContext.createDataFrame(dxData, schemaDx)
    dxDataSchemaRDD.registerTempTable("dx")

    //Encounters
    val encData = sc.textFile("/lpi/synth_data_rand_v1/encounters.txt.gz").map(_.split("\t")).map(p => Row(p(0), p(1), p(2), p(3), p(4), p(5)))
    val schemaEnc = StructType(schemaNamesEncs.split(" ").map(fieldName => StructField(fieldName, StringType, true)))
    val encDataSchemaRDD = hiveContext.createDataFrame(encData, schemaEnc)
    encDataSchemaRDD.registerTempTable("encs")

    //Military
    val milData = sc.textFile("/lpi/synth_data_rand_v1/MilitaryService.txt").map(_.split("\t")).map(p => Row(p(0), p(1), p(2), p(3)))
    val schemaMil = StructType(schemaNamesEncs.split(" ").map(fieldName => StructField(fieldName, StringType, true)))
    val milDataSchemaRDD = hiveContext.createDataFrame(milData, schemaMil)
    milDataSchemaRDD.registerTempTable("military")

    //Suicide encounter IDs
    val suicideDX = createListFromFile("/suicideDx")
    val suicideIDs = dxDataSchemaRDD.sqlContext.sql("SELECT encounterID, diagnosisCode from dx").where("diagnosisCode" .isin(suicideDX:_*)).select("encounterID").rdd.map(r => r(0).asInstanceOf[Int]).collect()

    //Gender
    def gender: DataFrame = patientDataSchemaRDD.sqlContext.sql("SELECT patientID, gender from patients").where($"patientID" .isin(suicideIDs:_*))

    //Age of enlistment
    val enlistment = milDataSchemaRDD.sqlContext.sql("SELECT patientID, promotionDate from military")
      .join(patientDataSchemaRDD, milDataSchemaRDD("patientID") === patientDataSchemaRDD("patientID"))
      .groupBy("patientID")
      .min("promotionDate")
      .withColumn("enlistAge", functions.datediff($"promotionDate", $"dob"))
      .withColumn("enlist27", functions.when($"enlistAge" <= 27, 1).otherwise(0))

    //Prior suicide attempt
    val priorAttempt = dxDataSchemaRDD.sqlContext
      .sql("SELECT encounterID, diagnosisCode from dx")
      .join(encDataSchemaRDD, dxDataSchemaRDD("encounterID") === encDataSchemaRDD("encounterID"))
      .where($"diagnosisCode" .isin(suicideDX:_*)).select("patientID")
      .rdd.map(r => r(0)).collect().toList
      .groupBy(x => x)
      .map{ case (k,v) => k -> v.length}
      .toDF("patientID": String, "mentalnum": String)

    //Number of outpatient encounters with ideation in last 12 months
    val ideationList = createListFromFile("/suicideIdeation")
    val outpt = createListFromFile("/outpatient") // environment variable, mimic all encounters are inpatient
    val ideation = dxDataSchemaRDD
        .sqlContext.sql("SELECT encounterID, diagnosisCode from dx")
        .join(encDataSchemaRDD, dxDataSchemaRDD("encounterID") === encDataSchemaRDD("encounterID"))
        .where($"diagnosisCode" .isin(suicideDX:_*))
        .where($"diagnosisCode" .isin(ideation:_*))
        .where(functions.datediff(functions.current_date(), $"admitDate") <= 360)
        .where($"encounterType" === "outpatient")
        .select("patientID")
        .rdd.map(r => r(0).asInstanceOf[Int]).collect().toList.groupBy(x => x).map{ case (k,v) => k -> v.length}

    //Number of outpatient encounters with mental health professional in last 12 months
    val mh = createListFromFile("/mentalHealthPro")
    val mental = dxDataSchemaRDD.sqlContext
      .sql("SELECT encounterID, diagnosisCode from dx")
      .join(encDataSchemaRDD, dxDataSchemaRDD("encounterID") === encDataSchemaRDD("encounterID"))
      .where($"diagnosisCode" .isin(suicideDX:_*))
      .where($"diagnosisCode" .isin(mh:_*))
      .where($"encounterType" === "outpatient")
      .where(functions.datediff(functions.current_date(), $"admitDate") <= 360)
      .select("patientID").rdd.map(r => r(0)).collect().groupBy(x => x).mapValues(x => x.length)

    //Number of antidepressants filled in last 12 months
    val antidepressants = createListFromFile("/antidepressants")
    val num_anti = medsDataSchemaRDD.sqlContext
      .sql("SELECT encounterID, medName from meds")
      .join(encDataSchemaRDD, medsDataSchemaRDD("encounterID") === encDataSchemaRDD("encounterID"))
      .where($"medName" .isin(antidepressants:_*))
      .where(functions.datediff(functions.current_date(), $"medOrderDate") <= 360)
      .select("patientID")
      .rdd.map(r => r(0).asInstanceOf[Int]).collect().toList.groupBy(x => x).map{ case (k,v) => k -> v.length}

    //Number of psychiatric hospitalizations / time in service > 50th percentile
    val psych = createListFromFile("/inptPscyhEnv")
    val psych_hosp = dxDataSchemaRDD.sqlContext
      .sql("SELECT encounterID, diagnosisCode from dx")
      .join(encDataSchemaRDD, dxDataSchemaRDD("encounterID") === encDataSchemaRDD("encounterID"))
      .where($"encounterType" === "inpatient")
      .where($"diagnosisCode" .isin(psych:_*))
      .select("patientID")
      .rdd.map(r => r(0).asInstanceOf[Int]).collect().toList.groupBy(x => x).map{ case (k,v) => k -> v.length}
    //val timeService =

    //Number of psychiatric treatment in last 12 months
//    val psych_hosp = dxDataSchemaRDD.sqlContext
//      .sql("SELECT encounterID, diagnosisCode from dx")
//      .join(encDataSchemaRDD, dxDataSchemaRDD("encounterID") === encDataSchemaRDD("encounterID"))
//      .where($"encounterType" === "inpatient")
//      .where($"diagnosisCode" .isin(psych:_*))
//      .where(functions.datediff(functions.current_date(), $"medOrderDate") <= 360)
//      .select("patientID")
//      .rdd.map(r => r(0).asInstanceOf[Int]).collect().toList.groupBy(x => x).map{ case (k,v) => k -> v.length}

    //Number of inpatient days for major depression in last 12 months
    val majDepres = createListFromFile("/majorDepression")
    val num_days_depress = dxDataSchemaRDD.sqlContext
      .sql("SELECT encounterID, diagnosisCode from dx")
      .join(encDataSchemaRDD, dxDataSchemaRDD("encounterID") === encDataSchemaRDD("encounterID"))
      .where($"encounterType" === "inpatient")
      .where($"diagnosisCode" .isin(majDepres:_*))
      .withColumn("daysDepress", functions.datediff($"dischargeDate", $"admitDate"))
      .select("patientID, daysDepress")
      //.groupBy($"patientID").map()

    //The combined dataframe
    def calculateModel(patientID: String): Array[Double] = {
      val modelValue: DataFrame = hiveContext.sql(
        "SELECT p.patientID, case  when g.gender == 'male' then 1, else 0 end as gender, e.enlistment, " +
          "pa.priorAttempt, i.ideation, m.mentalhealth" +
          "FROM patients p LEFT OUTER JOIN gender g ON p.patientID = g.patientID," +
          "patients p LEFT OUTER JOIN enlistment e ON p.patientID = e.patientID" +
          "patients p LEFT OUTER JOIN priorAttempt pa ON p.patientID = pa.patientID"+
          "patients p LEFT OUTER JOIN ideation i ON p.patientID = i.patientID"+
          " "
      )

    }
  }
}
