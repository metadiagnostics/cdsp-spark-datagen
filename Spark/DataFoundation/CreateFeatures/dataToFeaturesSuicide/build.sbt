
name := "dataToFeaturesSuicide"

version := "1.0"

scalaVersion := "2.10.6"

resolvers ++= Seq(
  "Apache" at "https://repository.apache.org/content/repositories/releases/"
)

libraryDependencies ++= Seq(
  "org.apache.hadoop" % "hadoop-client" % "2.5.2" excludeAll(
    ExclusionRule(organization = "com.sun.jdmk"),
    ExclusionRule(organization = "com.sun.jmx"),
    ExclusionRule(organization = "javax.jms"),
    ExclusionRule(organization = "javax.servlet")) withSources() withJavadoc(),
  "org.scalatest" % "scalatest_2.10" % "2.2.6" % "test",
  "org.apache.spark" %% "spark-sql" % "1.6.1" withSources() withJavadoc(),
  "org.apache.spark" %% "spark-core" % "1.6.1"  withSources() withJavadoc(),
  "com.github.nscala-time" %% "nscala-time" % "2.12.0",
  "org.apache.hbase" % "hbase-client" % "0.98.12-hadoop2" excludeAll(
    ExclusionRule(organization = "com.sun.jdmk"),
    ExclusionRule(organization = "com.sun.jmx"),
    ExclusionRule(organization = "javax.jms"),
    ExclusionRule(organization = "javax.servlet")) withSources() withJavadoc(),
  "org.apache.hbase" % "hbase-common" % "0.98.12-hadoop2" excludeAll(
    ExclusionRule(organization = "com.sun.jdmk"),
    ExclusionRule(organization = "com.sun.jmx"),
    ExclusionRule(organization = "javax.jms"),
    ExclusionRule(organization = "javax.servlet")) withSources() withJavadoc(),
  "org.apache.spark" %% "spark-hive" % "1.6.1"
)