/*
 *
 * Copyright 2016 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *
 */


import org.apache.hadoop.security.UserGroupInformation
import org.apache.spark.{SparkConf, SparkContext}
import org.joda.time.{DateTime, Days}
import org.joda.time.format.DateTimeFormat
import java.sql.Timestamp

import scala.io.Source
import org.apache.spark.sql._
import org.apache.spark.sql.types._
import org.apache.spark.sql.functions.unix_timestamp
import org.apache.hadoop.hbase.client.{HBaseAdmin, HTable}
import org.apache.hadoop.hbase.protobuf.generated.RPCProtos.ExceptionResponseOrBuilder

//import org.apache.hadoop.fs.Path
//import org.apache.hadoop.hbase.HBaseConfiguration

/**
  * The purpose of this application is to transform electronic medical record data into features (parameters) for a
  * previously discovered or defined statistical model.
  * The specific statistical model is for percutaneus coronary intervention (PCI). (This model is fictitious and in
  * no way should it be used to predict, classify, or guide decisions on PCI diagnosis, prognosis, or treatment)
  * The EMR data is assumed to consists of medication orders, laboratory test orders and results, procedure orders.
  * The features of the PCI model are:
  * 1. being female, 2. age, 3. number of comorbid conditions, 4. length of stay, 5. severity
  * Severity will be defined as the number of arteries blocked
  *
  * Created by wblack on 4/13/16.
  */

object dataToFeaturesPCI {
  val realUser = UserGroupInformation.createRemoteUser("wblack1")
  UserGroupInformation.setLoginUser(realUser)
  val conf = new SparkConf().setAppName("dataToFeaturesPCI").setMaster("local")
  val sc = new SparkContext(conf)

  def main(args: Array[String]) {
    // Set sqlContext, will use new org.apache.spark.hive.HiveContext(sc)
    val sqlContext = new org.apache.spark.sql.SQLContext(sc)

    import sqlContext.implicits._

    def getTimestamp(s: String): Option[Timestamp] = s match {
      case "" => None
      case null => None
      case _ => {
        try {
          val format = DateTimeFormat.forPattern("MM/dd/yyyy hh:mm a")
          Some(new Timestamp(format.parseDateTime(s).getMillis))
        } catch {
          case e: Exception => None
        }
      }
    }

    //Schemas
    val schemaNamesPt = "patientID firstName lastName gender ptdob"
    val schemaNamesMeds = "encounterID medOrderID medName medDose medDoseUnits medQuantity medID medOrderDate medOrderComplete"
    val schemaNamesProcs = "encounterID procOrderID procName procDate"
    val schemaNamesDx = "encounterID diagnosisID diagnosisCode diagnosisDescription"
    val schemaNamesEncs = "patientID encounterID admitDate dischargeDate encounterType facilityID"

    // Read in the patient data including dob

    val patientData = sc.textFile("/Users/asimms/Dev/synthetic/projects/datagen/Spark/DataFoundation/CreateFeatures/dataToFeaturesPCI/src/test/resources/patients.txt").map(_.split("\t")).map(p => Row(p(0), p(1).trim, p(2), p(3), p(4)))
    // Create a schema from the schema string for patient data
    val schemaPt = StructType(schemaNamesPt.split(" ").map(fieldName => StructField(fieldName, StringType, true)))
    // Apply schema to patient data
    val patientDataSchemaRDD = sqlContext.createDataFrame(patientData, schemaPt)
    patientDataSchemaRDD.registerTempTable("patients")

    //Medications
    //val medicationData = sc.textFile("/Users/wblack/Desktop/Junk_Data_Generator/Data_Generated/medications.txt").map(_.split("\t")).map(p => Row(p(0), p(1).trim, p(2), p(3), p(4), p(5), p(6), p(7), p(8)))
    val medicationData = sc.textFile("/Users/asimms/Dev/synthetic/projects/datagen/Spark/DataFoundation/CreateFeatures/dataToFeaturesPCI/src/test/resources/medications.txt").map(_.split("\t")).map(p => Row(p(0), p(1), p(2), p(3), p(4), p(5), p(6), p(7), p(8)))
    val schemaMeds = StructType(schemaNamesMeds.split(" ").map(fieldName => StructField(fieldName, StringType, true)))
    val medsDataSchemaRDD = sqlContext.createDataFrame(medicationData, schemaMeds)
    medsDataSchemaRDD.registerTempTable("meds")

    //Procedures
    val procData = sc.textFile("/Users/asimms/Dev/synthetic/projects/datagen/Spark/DataFoundation/CreateFeatures/dataToFeaturesPCI/src/test/resources/procedures.txt").map(_.split("\t")).map(p => Row(p(0), p(1), p(2), p(3)))
    val schemaProc = StructType(schemaNamesProcs.split(" ").map(fieldName => StructField(fieldName, StringType, true)))
    val procsDataSchemaRDD = sqlContext.createDataFrame(procData, schemaProc)
    procsDataSchemaRDD.registerTempTable("procs")

    //Diagnosis
    val dxData = sc.textFile("/Users/asimms/Dev/synthetic/projects/datagen/Spark/DataFoundation/CreateFeatures/dataToFeaturesPCI/src/test/resources/diagnoses.txt").map(_.split("\t")).map(p => Row(p(0), p(1), p(2), p(3)))
    val schemaDx = StructType(schemaNamesDx.split(" ").map(fieldName => StructField(fieldName, StringType, true)))
    val dxDataSchemaRDD = sqlContext.createDataFrame(dxData, schemaDx)
    dxDataSchemaRDD.registerTempTable("dx")


    //Encounters
    val encData = sc.textFile("/Users/asimms/Dev/synthetic/projects/datagen/Spark/DataFoundation/CreateFeatures/dataToFeaturesPCI/src/test/resources/encounters.txt").map(_.split("\t")).map(
      p => Row(p(0), p(1), getTimestamp(p(2)).getOrElse(null), getTimestamp(p(3)).getOrElse(null), p(4), p(5)))
    val schemaEnc = StructType(schemaNamesEncs.split(" ").map(fieldName => fieldName match {
      case "admitDate" | "dischargeDate" => StructField(fieldName, TimestampType, true)
      case any => StructField(fieldName, StringType, true)
    }))
    val encDataSchemaRDD = sqlContext.createDataFrame(encData, schemaEnc)
    encDataSchemaRDD.registerTempTable("encs")

    // Values in resource files to list
    val copdMeds = createListFromFile("/copdMeds")
    val copdDx = createListFromFile("/copdDx")
    val copdProcs = createListFromFile("/copdProcs")
    val chfDx = createListFromFile("/chfDx")
    val chfMeds = createListFromFile("/chfMeds")
    val chfProcs = createListFromFile("/chfProcs")
    val strokeDx = createListFromFile("/StrokeDx")
    val strokeMeds = createListFromFile("/StrokeMeds")
    val strokeProcs = createListFromFile("/StrokeProc")
    val pciDx = createListFromFile("/pciDx")
    val pciProc = createListFromFile("/pciProc")

    //List of PCI encounters
    val pciEncDx = dxDataSchemaRDD.sqlContext.sql("SELECT encounterID, diagnosisCode from dx").where($"diagnosisCode".isin(pciDx: _*))
    pciEncDx.show(15)

    //val pciEncIDs = pciEncDx.select("encounterID").rdd.map(r => r(0).asInstanceOf[Int]).collect()
    val pciEncIDs = pciEncDx.select("encounterID").rdd.map(r => r(0) ).collect()

    println("pciDx")
    println(pciDx)

    val test = pciEncDx.select("encounterID").groupBy($"encounterID").count().distinct()
    test.show()


    //Age
    val age = patientDataSchemaRDD.sqlContext.sql("SELECT patientID, ptdob FROM patients").where($"patientID".isin(pciEncIDs: _*)).map(x => Row.apply(duration("ptdob")))

    // Gender
    val gender = patientDataSchemaRDD.sqlContext.sql("SELECT patientID, gender from patients").where($"patientID".isin(pciEncIDs: _*))
    // val female = gender.filter (when female) -- patientId, gender (all female)

    // Number of comorbid conditions
    val comorbid_copdDx = dxDataSchemaRDD.sqlContext.sql("SELECT encounterID, diagnosisCode from dx").where($"diagnosisCode".isin(copdDx: _*)).select("encounterID").rdd.map(r => r(0).asInstanceOf[String].toInt).collect()

    println("comorbid_copdDx")
    println(comorbid_copdDx.foreach(x => printf(s"  ${x} ${x.getClass.toString}")))

    val comorbid_copdMeds = medsDataSchemaRDD.sqlContext.sql("SELECT encounterID, medName from meds").where($"medName".isin(copdMeds: _*)).select("encounterID").rdd.map(r => r(0).asInstanceOf[String].toInt).collect()
    val comorbid_copdProcs = medsDataSchemaRDD.sqlContext.sql("SELECT encounterID, procName from procs").where($"procName".isin(copdProcs: _*)).select("encounterID").rdd.map(r => r(0).asInstanceOf[String].toInt).collect()
    val comorbid_copd = List(comorbid_copdDx, comorbid_copdMeds, comorbid_copdProcs).flatten.distinct
    val comorbid_chfDx = dxDataSchemaRDD.sqlContext.sql("SELECT encounterID, diagnosisCode from dx").where($"diagnosisCode".isin(chfDx: _*)).select("encounterID").rdd.map(r => r(0).asInstanceOf[String].toInt).collect()
    val comorbid_chfMeds = medsDataSchemaRDD.sqlContext.sql("SELECT encounterID, medName from meds").where($"medName".isin(chfMeds: _*)).select("encounterID").rdd.map(r => r(0).asInstanceOf[String].toInt).collect()
    val comorbid_chfProcs = medsDataSchemaRDD.sqlContext.sql("SELECT encounterID, procName from procs").where($"procName".isin(chfProcs: _*)).select("encounterID").rdd.map(r => r(0).asInstanceOf[String].toInt).collect()
    val comorbid_chf = List(comorbid_chfDx, comorbid_chfMeds, comorbid_chfProcs).flatten.distinct
    val comorbid_StrokeDx = dxDataSchemaRDD.sqlContext.sql("SELECT encounterID, diagnosisCode from dx").where($"diagnosisCode".isin(strokeDx: _*)).select("encounterID").rdd.map(r => r(0).asInstanceOf[String].toInt).collect()
    val comorbid_StrokeMeds = medsDataSchemaRDD.sqlContext.sql("SELECT encounterID, medName from meds").where($"medName".isin(strokeMeds: _*)).select("encounterID").rdd.map(r => r(0).asInstanceOf[String].toInt).collect()
    val comorbid_StrokeProcs = medsDataSchemaRDD.sqlContext.sql("SELECT encounterID, procName from procs").where($"procName".isin(strokeProcs: _*)).select("encounterID").rdd.map(r => r(0).asInstanceOf[String].toInt).collect()
    val comorbid_Stroke = List(comorbid_StrokeDx, comorbid_StrokeMeds, comorbid_StrokeProcs).flatten.distinct
    val comorbids = List(comorbid_chf, comorbid_copd, comorbid_Stroke).flatten.groupBy(x => x).map { case (k, v) => k -> v.length }

    encDataSchemaRDD.printSchema()
    encDataSchemaRDD.show(15)

    printf("Comorbid Count Total: %d\n".format(comorbids.size))
    comorbids.keys.foreach{ i => printf("  Encounter %d: %d\n".format(i,comorbids(i)))}

    //Length of stay
    val lengthOfStay = encDataSchemaRDD.select("encounterID", "admitDate", "dischargeDate").where($"encounterID".isin(pciEncIDs: _*)).withColumn("los", functions.datediff(encDataSchemaRDD("dischargeDate"), encDataSchemaRDD("admitDate")))
    //val lengthOfStay = encDataSchemaRDD.select("encounterID", "admitDate", "dischargeDate").where($"encounterID".isin(pciEncIDs: _*)).withColumn("admitTS", unix_timestamp($"admitDate", "MM/dd/yyy hh:mm a").cast("timestamp")).withColumn("dischargeTS", unix_timestamp($"dischargeDate", "MM/dd/yyy hh:mm a").cast("timestamp")).withColumn("los", functions.datediff($"dischargeTS", $"admitTS"))

    lengthOfStay.show(15)
    //Severity

    // PCI Model -- drop existing table, recreate, fill as below (think of one really great name for this) JonathanSucksPCIFeaturesTable
    // Note, may need to switch context to hive in order to create tables
    // Final should be a single feature table, with a column for each feature
    // 0. patient_id 1. being female, 2. age, 3. number of comorbid conditions, 4. length of stay, 5. severity

    // All Dx codes that went into the feature set (like staging tables on the cluster)
    // Needs only Dx relevance to the model, all dx that had anything to do with comorbid conditions

    // All Encounters, meds, labs, patient demo, notes, facilities, military service

    sc.stop()

  }

  def duration(dob: String): Float = {
    val dt = new DateTime(dob)
    val currentdt = DateTime.now()
    val duration_years = (Days.daysBetween(dt, currentdt).getDays).toFloat / 365
    duration_years
  }


  def createListFromFile(resoursepath: String): List[String] = {
    val outList = Source.fromInputStream(getClass.getResourceAsStream(resoursepath)).getLines().toList
    outList
  }


}
