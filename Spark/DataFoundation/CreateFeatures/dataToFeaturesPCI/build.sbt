
name := "dataToFeaturesPCI"

version := "1.0"

scalaVersion := "2.10.6"

resolvers ++= Seq(
  //"MapR jars" at "http://repository.mapr.com/nexus/content/groups/mapr-public/",
  //"MapR External Releases"  at "http://repository.mapr.com/nexus/content/repositories/releases/",
  "Apache" at "https://repository.apache.org/content/repositories/releases/"
)

libraryDependencies ++= Seq(
  "org.apache.hadoop" % "hadoop-client" % "2.5.2" excludeAll(
    ExclusionRule(organization = "com.sun.jdmk"),
    ExclusionRule(organization = "com.sun.jmx"),
    ExclusionRule(organization = "javax.jms"),
    ExclusionRule(organization = "javax.servlet")) withSources() withJavadoc(),
  //  "org.apache.hadoop" % "hadoop-client" % "2.5.1-mapr-1501" excludeAll(
  //    ExclusionRule(organization = "com.sun.jdmk"),
  //    ExclusionRule(organization = "com.sun.jmx"),
  //    ExclusionRule(organization = "javax.jms"),
  //    ExclusionRule(organization = "javax.servlet")),
  "org.scalatest" %% "scalatest" % "2.2.6" % "test",
  "org.apache.spark" %% "spark-sql" % "1.6.1" withSources() withJavadoc(),
  "org.apache.spark" %% "spark-core" % "1.6.1" withSources() withJavadoc(),
  "com.github.nscala-time" %% "nscala-time" % "2.12.0",
  "org.apache.hbase" % "hbase-client" % "0.98.12-hadoop2" excludeAll(
    ExclusionRule(organization = "com.sun.jdmk"),
    ExclusionRule(organization = "com.sun.jmx"),
    ExclusionRule(organization = "javax.jms"),
    ExclusionRule(organization = "javax.servlet")) withSources() withJavadoc(),
  "org.apache.hbase" % "hbase-common" % "0.98.12-hadoop2" excludeAll(
    ExclusionRule(organization = "com.sun.jdmk"),
    ExclusionRule(organization = "com.sun.jmx"),
    ExclusionRule(organization = "javax.jms"),
    ExclusionRule(organization = "javax.servlet")) withSources() withJavadoc()
  //  "org.apache.hbase" % "hbase-client" % "0.98.12-mapr-1506-m7-5.0.0" excludeAll(
  //    ExclusionRule(organization = "com.sun.jdmk"),
  //    ExclusionRule(organization = "com.sun.jmx"),
  //    ExclusionRule(organization = "javax.jms"),
  //    ExclusionRule(organization = "javax.servlet")),
  //  "org.apache.hbase" % "hbase-common" % "0.98.12-mapr-1506-m7-5.0.0" excludeAll(
  //    ExclusionRule(organization = "com.sun.jdmk"),
  //    ExclusionRule(organization = "com.sun.jmx"),
  //    ExclusionRule(organization = "javax.jms"),
  //    ExclusionRule(organization = "javax.servlet"))
)


