/*
 *
 *  * Copyright 2016 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *  *
 *  * Licensed under the Apache License, Version 2.0 (the "License");
 *  * you may not use this file except in compliance with the License.
 *  * You may obtain a copy of the License at
 *  *
 *  * http://www.apache.org/licenses/LICENSE-2.0
 *  *
 *  * Unless required by applicable law or agreed to in writing, software
 *  * distributed under the License is distributed on an "AS IS" BASIS,
 *  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  * See the License for the specific language governing permissions and
 *  * limitations under the License.
 *
 *
 */
import org.apache.commons.codec.binary.Base64
import java.io._

import org.apache.commons.httpclient.UsernamePasswordCredentials
import org.apache.http.auth.AuthScope
import org.apache.http.client._
import org.apache.http.HttpHeaders
import org.apache.http.client.methods.HttpPost
import org.apache.http.impl.client.{BasicCredentialsProvider, DefaultHttpClient}
import org.apache.http.params.HttpConnectionParams
import org.apache.http.params.HttpParams
import scala.io.Source

/**
  * From Scala Cookbook
  * Returns the test (content) from a REST URL as a String
  * Returns a blank string if there was a problem. This function will also throw exceptions if there are problems
  * trying to connect to the URL
  *
  * Created by wblack on 3/21/16.
  */
class connectAllegro(url:String) {

  val basic = "BASIC"
  val Authorization = "Authorization"
  val usrname = "lpi"
  val pwd = "XXXXXX"
  def encodeCred(username: String, password: String): String={
    new String(Base64.encodeBase64((username + ":" + password).getBytes))
  }
  def getAuthHeader(username: String, password: String): String={
    basic + " " + encodeCred(username, password)
  }
  def getRestContent(connectionTimeout: Int, socketTimeout: Int) : String={
    val httpClient = buildHttpClient(connectionTimeout, socketTimeout)
    val httpRequest= new HttpPost(url)
    httpRequest.setHeader(HttpHeaders.AUTHORIZATION, getAuthHeader(usrname, pwd))
    httpRequest.setHeader("Content-type", "application/json")
    val httpResponse = httpClient.execute(httpRequest)
    val entity = httpResponse.getEntity
    var content = ""
    if(entity != null){
      val inputStream= entity.getContent
      content = scala.io.Source.fromInputStream(inputStream).getLines().mkString
      inputStream.close
    }
    httpClient.getConnectionManager.shutdown
    content
  }

  private def buildHttpClient(connectionTimeout: Int, socketTimeout: Int): DefaultHttpClient={
    val httpClient = new DefaultHttpClient
    val httpParams = httpClient.getParams
    HttpConnectionParams.setConnectionTimeout(httpParams, connectionTimeout)
    HttpConnectionParams.setSoTimeout(httpParams, socketTimeout)
    httpClient.setParams(httpParams)
    httpClient
  }
}
