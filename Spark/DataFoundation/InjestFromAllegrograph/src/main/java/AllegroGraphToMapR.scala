/*
 *
 *  * Copyright 2016 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *  *
 *  * Licensed under the Apache License, Version 2.0 (the "License");
 *  * you may not use this file except in compliance with the License.
 *  * You may obtain a copy of the License at
 *  *
 *  * http://www.apache.org/licenses/LICENSE-2.0
 *  *
 *  * Unless required by applicable law or agreed to in writing, software
 *  * distributed under the License is distributed on an "AS IS" BASIS,
 *  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  * See the License for the specific language governing permissions and
 *  * limitations under the License.
 *
 *
 */
import java.io.{BufferedWriter, File, FileWriter}
import org.apache.spark.{SparkConf, SparkContext}

/**
  * Created by wblack on 3/21/16.
  */

  object AllegroGraphToMapR{
    val conf = new SparkConf().setMaster("yarn").setAppName("AllegroGraphToMapR")
    new SparkContext(conf)

  def Main(args: Array[String]){
      val connectAllegro = new connectAllegro("http://lpitriple:10035/")
      val outfile= connectAllegro.getRestContent(5000, 5000)

  }
}







