# DataGen Project

The DataGen Project is an open source collection of utilities for producing 
simulated patient data.  Utilities in the Python and R directories are intended
to run on a single computer.  Utilities in the Spark directory are designed to 
be run on a hadoop cluster.  Python and R tools run without compilation Spark 
examples must be built using SBT (http://www.scala-sbt.org/)

* Python -- scripts to build some specific data types (e.g. encounters) using the Python language
* R -- R code to create some example table structures (e.g. feature tables)
* Spark -- Parallel distributed leveraging a hadoop and spark to create features and data structures
* DrillSQL -- Commands that describe the creation of tables from Cluster Data
* Documentation  -- Documentation

-------

