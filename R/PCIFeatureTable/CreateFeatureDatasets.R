# The purpose of this script is the generate synthetic feature and outcome data for the LPI use case models.
# This data will be used to test the Cohort Identification Service
#
# Note: Replace <PROJDIR> with your Project Directory

#The PCI readmission model 
# the risk factors are female, age, comorbid conditions, length of stay, severity of disease

# The logistic model
pci_model <- pci_readmit ~ female + comorbid + length_stay + severity
# The linear model
pci_linear_model <- outcome ~ female + comorbid + length_stay + severity

#Generate data for the PCI Readmission model. The covariates are ficticious.
# The number of patients in the dataset
popSize = 3000000 

# Generate the independent variables
female <- rbinom(popSize,1,.5)
comorbid <- sample(0:5, popSize, replace=TRUE, prob = c(0.98,0.03,0.02,0.001, 0.0005, 0.0001))
length_stay <- sample(0:10, popSize, replace = TRUE, prob = c(0.98, 0.1, 0.075, 0.03, 0.02, 0.005, 0.003, 0.002, 0.001, 0.0005, 0.0001))
severity<- sample(0:3, popSize, replace = TRUE, prob = c(0.99, 0.06, 0.03, 0.01))

# Generate encounter number, which is a sequence of 1 to the population size
encounterID <- 1:popSize

# Generate the outcome using the binomial distribution
z = -2.4 + 0.2*female + 0.5*comorbid + 0.7*length_stay + 0.2*severity
pr = 1/(1+exp(-z))
pci_readmit <- rbinom(popSize, 1, pr)

#Generate the outcome using linear model with normally distributed random error
outcome = -2.4 + 0.2*female + 0.5*comorbid + 0.7*length_stay + 0.2*severity + rnorm(popSize)


# Generate the score
score <- -2.4 + 0.2*female + 0.5*comorbid + 0.7*length_stay + 0.2*severity

# The data which includes the logistic model outcome, the raw model score, and the independent variables,
# Changing encounter ID to patient ID because the CIS application can't process more than one encounter per patient
pci_synthetic_data <- data.frame(patientID=encounterID, pci_readmit=pci_readmit,score = score, probability = pr, female = female, comorbid = comorbid, length_stay = length_stay, severity = severity)

# The logistic model and linear model
glm(formula = pci_model, data = pci_synthetic_data, family ="binomial")
lm(formula = pci_linear_model, data = pci_synthetic_data)
lm(formula = pci_model, data = pci_synthetic_data)

# Write data out to file
write.table(pci_synthetic_data, file = "<PROJDIR>/Junk_Data_Generator/PCI_Feature_Data3million.txt", sep= "\t", row.names = FALSE, quote = FALSE)

#########################################################################################################################
#The suicide risk model
# the risk factors are male, enlistment age >= 27, 
#prior suicide attempt
# number of outpt visits with suicidal ideation in past 12 months
#number of outpt mental health professional visits in last 12 months
#number antidepressant precriptions filled in last 12 months
# number of psychiatric hospitalizations/time in service >50th percentile
# any prior inpat psychiatric treatment in past 12 months
# number of inpt days in past 12 months for major depression
# somatoform or disassociative disorder
# diagnosis of ptsd or depression
# substance misuse (alcohol)

# The logistic model
suicide_model <- suicideRisk ~ male + enlist_age27 + prior_attempt + numOutpt_ideation + num_Antidepressants + num_psych_hosp + prior_inpt_psych + num_days_depression + somatoform_dissoc + ptsd_depression + substance_alcohol

# The linear model
suicide_linear_model <- suicide_outcome ~ male + enlist_age27 + prior_attempt + numOutpt_ideation + num_Antidepressants + num_psych_hosp + prior_inpt_psych + num_days_depression + somatoform_dissoc + ptsd_depression + substance_alcohol

# Generate data for sucicide model, the covariates are ficticious.
# The number of patients in the dataset
popSize = 3000000 

# The independent variables
male <- rbinom(popSize, 1, 0.5)
elist_age27 <- rbinom(popSize,1, 0.2)
prior_attempt <- rbinom(popSize,1,0.01)
numOutpt_ideation <- sample(0:2, popSize, replace= TRUE, prob = c(0.99, 0.1, 0.005))
num_Antidepressants <- sample(0:4, popSize, replace=TRUE, prob = c(0.99, 0.4, 0.01, 0.005, 0.001))
num_psych_hosp <- sample(0:2, popSize, replace = TRUE, prob = c(0.99, 0.01, 0.005))
prior_inpt_psych <-rbinom(popSize, 1, 0.1)
num_days_depression <- sample(0:5, popSize, replace=TRUE, prob = c(0.95, 0.2, 0.3, 0.1, 0.004, 0.001))
somatoform_dissoc<- rbinom(popSize, 1, 0.01)
ptsd_depression <- rbinom(popSize, 1, 0.3)
substance_alcohol <- rbinom(popSize, 1, 0.2)

# Generate patient number, which is a sequence of 1 to the population size
patient <- 1:popSize

# Generate outcomes using binomial distribution
z = -15 + 0.5*male + 0.1*elist_age27 + 1.2*prior_attempt + 1.8*numOutpt_ideation + 1.02*num_Antidepressants + 2*num_psych_hosp + 1.4*prior_inpt_psych + 1.7*num_days_depression + 2.0*somatoform_dissoc + 2.4*ptsd_depression + 0.2*substance_alcohol
pr = 1/(1+exp(-z))
suicideRisk<- rbinom(popSize, 1, pr)

# Generate outcomes using linear model with normally distributed error
suicide_outcome <- -15 + 0.5*male + 0.1*elist_age27 + 1.2*prior_attempt + 1.8*numOutpt_ideation + 1.02*num_Antidepressants + 2*num_psych_hosp + 1.4*prior_inpt_psych + 1.7*num_days_depression + 2.0*somatoform_dissoc + 2.4*ptsd_depression + 0.2*substance_alcohol + rnorm(popSize)

# Generate raw model score
score <- -15 + 0.5*male + 0.1*elist_age27 + 1.2*prior_attempt + 1.8*numOutpt_ideation + 1.02*num_Antidepressants + 2*num_psych_hosp + 1.4*prior_inpt_psych + 1.7*num_days_depression + 2.0*somatoform_dissoc + 2.4*ptsd_depression + 0.2*substance_alcohol

# The data for the suicide model which includes the logistic outcome, the raw score, and the independent variables
suicide_synthetic_data <- data.frame(patientID=patient, suicideRisk=suicideRisk, score=score, probability = pr,  male=male, enlist_age27=elist_age27,prior_attempt=prior_attempt, numOutpt_ideation = numOutpt_ideation,num_Antidepressants=num_Antidepressants, num_psych_hosp = num_psych_hosp, prior_inpt_psych=prior_inpt_psych, num_days_depression=num_days_depression, somatoform_dissoc=somatoform_dissoc,ptsd_depression=ptsd_depression, substance_alcohol = substance_alcohol)

# The logistic model and linear model
glm(formula = suicide_model, data = suicide_synthetic_data, family ="binomial")
lm(formula = suicide_linear_model, data = suicide_synthetic_data)
lm(formula = suicide_model, data = suicide_synthetic_data)

# Write data out to file
write.table(suicide_synthetic_data, file = "<PROJDIR>/Junk_Data_Generator/Suicide_Feature_Data3million.txt", sep= "\t", row.names = FALSE, quote = FALSE)
ftable(suicide_synthetic_data$suicideRisk )
length(unique(suicide_synthetic_data$patientID))
