import csv
import random
import multiprocessing as mp
import time

# Store encounter numbers and diagnosis codes and descriptions
encounters = []
diagnosisCodes = {}


def selectDx():
    """ Randomly select, without replacement 3 diagnosis codes and associated descriptions
    :return: list of lists of dx codes and descriptions
    """
    dxSelect = random.sample(diagnosisCodes.items(), 3)
    return dxSelect


def dxCodeworker(q, encID, dxID):
    """
    Randomly select 3 diagnosis codes and descriptions for the encounter. The function uses the dxID to
    calculate the id number (like a primary key in a db). The sequence number is the order of importance of
    the dx code.
    :param q: the queue
    :param encID: the encounter ID number
    :param dxID: the diagnosis code ID number
    :return: the string of the encounter dx code and description
    """
    dxout=[]
    id = dxID - 2
    sequenceNum = 0
    for dxDescription in selectDx():
        sequenceNum +=1
        dx = "\t".join(str(x) for x in [encID, id, dxDescription[0], dxDescription[1], sequenceNum]) + "\n"
        dxout.append(dx)
        id += 1
    q.put(dxout)
    return dxout


def outputworker(q, outfile):
    """
    Write the diagnosis codes to file. This function take a job object from the queue. Each object consists of
    a list of 3 strings. each string is one diagnosis code and description for an encounter. The function writes
    the string to the outfile.
    :param q: queue
    :param outfile: file
    :return: no return value
    """
    f = open(outfile, 'wb')
    while True:
        dxout = q.get()
        if dxout == "DONE":
            break
        for dxString in dxout:
            f.write(dxString)
            f.flush()
    f.close()

def main():
    # Read in the encounters, the file is tab delimited
    with open('/Users/chahn/Projects/data/DataGenerated/encounters.txt') as f:
        csvReader = csv.reader(f, delimiter="\t")
        for encounter in csvReader:
            encounters.append(encounter[1])

    # Read in the diagnosis codes and descriptions, the file is tab delimited
    with open('/Users/chahn/Projects/data/DataSources/diagnosis.csv') as f:
        csvReader = csv.reader(f, delimiter='\t')
        for diagnosis in csvReader:
            diagnosisCodes[diagnosis[0]] = diagnosis[1]

    # Diagnosis file location
    dxFile = '/Users/chahn/Projects/data/DataGenerated/diagnoses.txt'

    print("Starting diagnosis codes " + time.strftime("%c"))
    # Processing pool and queue
    manager = mp.Manager()
    q = manager.Queue()
    pool = mp.Pool(mp.cpu_count())

    #  Start adding the workers (objects that will write out the data) to the queue
    watcher = pool.apply_async(outputworker, (q, dxFile,))
    dxID = 1

    # Create objects to generate data and add to queue.
    # Each object generates 3 dx codes and associated descriptions
    # The dxID therefore increments by multiplying the encounter number by 3
    dxjobs=[]
    for encounter in encounters:
        encID = encounter
        dxID = int(encID)*3
        dxjob = pool.apply_async(dxCodeworker, (q, encID, dxID,))
        dxjobs.append(dxjob)
    for job in dxjobs:
        job.get()

    # Close the queue
    q.put("DONE")

    # Close processing pool
    pool.close()
    pool.join()

    print("End of dx codes " + time.strftime("%c"))

if __name__ == "__main__":
    main()



