from __future__ import print_function, with_statement
import multiprocessing
import random
import time
from datetime import datetime, timedelta
import loremipsum


def stringToTime(stringTime):
    """ Take a string and return a date and time formatted as 12/24/2016 12:45 AM
    :param stringTime: the string date and time
    :return: formatted date time string
    """
    format = '%m/%d/%Y %I:%M %p'
    timeFromString = time.mktime(time.strptime(stringTime, format))
    return timeFromString


def generateDate(startDate, endDate):
    """ Returns a random date between a starting date and ending date. The returned date is in
    the format of 12/24/2016 12:45 AM
    :param startDate: the starting date
    :param endDate: the end date
    :return: a randomly selected date between the start date and end date
    """
    format = '%m/%d/%Y %I:%M %p'
    startTime = stringToTime(startDate)
    endTime = stringToTime(endDate)
    randDate = startTime + random.random() * (endTime - startTime)
    return time.strftime(format, time.localtime(randDate))


def generateIntervalDate(admitDate):
    """Generate a date of a random interval from the admit date
    :param admitDate: user supplied date to start interval
    :return: a date after the user supplied date
    """
    format = '%m/%d/%Y %I:%M %p'
    intervaldays = random.randint(1, 20)
    intervalhrs = random.randint(1, 12)
    disch = (datetime.strptime(admitDate, format) + timedelta(days=intervaldays) + timedelta(hours=intervalhrs))
    return disch.strftime(format)


def generateDOB():
    """Generate a date of birth for synthetic patients, the start date is 1/1/1935 to 12/28/2000
    :return: A generated date of birth
    """
    format = '%m/%d/%Y'
    mon = random.randint(1, 12)
    d = random.randint(1, 28)
    yr = random.randint(1935, 2000)
    dob = datetime(year=yr, month=mon, day=d)
    dob_f = dob.strftime(format)
    return dob_f


def generateSentences():
    """Generate a synthetic notes for the synthetic encounters
    :return: A random number of lorum ipsum sentences
    """
    outStringList = loremipsum.get_sentences(random.randint(2, 6), True)
    outString = " ".join(outStringList)
    return outString


def generateMedName():
    """Select a random name from a list of 200 medications
    :return: A randomly selected medical name
    """
    medname_file = "/Users/wblack/Desktop/Junk Data Generator/Data sources/MedNamesList.txt"
    meds = open(medname_file).read().splitlines()
    return random.choice(meds)


def generateProcName():
    """Select a random name from a list of medical surgical and diagnostic procedures
    :return: A randomly selected procedure name
    """
    procname_file = "/Users/wblack/Desktop/Junk Data Generator/Data sources/ProcedureNamesList.txt"
    procnames = open(procname_file).read().splitlines()
    return random.choice(procnames)

def generateLabName():
    """Select a random name from a list of laboratory test names
    :return: A randomly selected laboratory test name
    """
    labname_file = "/Users/wblack/Desktop/Junk Data Generator/Data sources/LabNamesList.txt"
    labs = open(labname_file).read().splitlines()
    return random.choice(labs)


def generateFirstName():
    """Select a random first name from a list of popular first names
    :return: A randomly selected first name
    """
    fname_file = "/Users/wblack/Desktop/Junk Data Generator/Data sources/FirstNamesList.txt"
    fnames = open(fname_file).read().splitlines()
    return random.choice(fnames)


def generateLastName():
    """Select a random last name from a list of popular last names
    :return: A randomly selected last name
    """
    lname_file = "/Users/wblack/Desktop/Junk Data Generator/Data sources/LastNamesList.txt"
    lnames = open(lname_file).read().splitlines()
    return random.choice(lnames)


def generateEncType():
    """Select a random type of encounter from a list of encounter types. The types of encounters are office, inpatient
    and ER
    :return: A randomly selected encounter type
    """
    return random.choice(["office", "inpatient", "ER"])


def tableSize(startSize, multiplicative):
    """Generate the number of orders in a table. The size of the table is a random number between the start size and
    the start size times the multiplicative.
    :param startSize: The start size is the lowest number of orders
    :param multiplicative: The order of magnitude larger than the start size the table should be
    :return: The size of the table
    """
    return startSize + random.randint(startSize, startSize*multiplicative)


def encounterTable(numberPatients, encTableFile, admitDic, dischDict):
    """Generate a file of encounters. The function randomly generates 1) the number of encounters based on the
    number of patients, 2)admission 3) discharge dates, 4) facility, and increments an encounter ID. The function
    returns the number of encounters and stores the admission date and discharge date
    :param numberPatients: the number of patients
    :param encTableFile: The location of the encounter data
    :param admitDic: the dictionary to store the generated admission dates
    :param dischDict: the dictionary to store the generated discharge dates
    :return: the number of encounters
    """
    numEncounters = tableSize(numberPatients, 3)
    with open(encTableFile, 'w') as outfile:
        encID = 0
        for enc in xrange(0, numEncounters):
            patientID = random.randint(1, numberPatients)
            encID += 1
            admitDate = generateDate("1/1/2000 1:00 AM", "12/10/2015 12:59 PM")
            dischargeDate = generateIntervalDate(admitDate)
            encType = generateEncType()
            facilityID = random.randint(1, 50)
            enc = "\t".join((str(x) for x in [patientID, encID, admitDate, dischargeDate, encType, facilityID])) + "\n"
            outfile.write(enc)
            admitDic[encID] = admitDate
            dischDict[encID] = dischargeDate
    outfile.close()
    return numEncounters


def medicationOrder(q, encID, medOrderID, admitDate, dischargeDate):
    """Create a single medication order. The function takes the queue the order should be added, the encounter ID,
    the medication order id, the admission date, and discharge date from the encounter. The function randomly selects
    1) the medication name, the dose, units, order date and order completed date. The entire generated order is then added
    to a queue for writing out to a file.
    :param q: the queue to add the completed order
    :param encID: the encounter id the medication order is associated with
    :param medOrderID: the id of the medication order
    :param admitDate: the admission date
    :param dischargeDate: the discharge date
    :return: the medication order as a tab delimited string
    """
    medName = generateMedName()
    dose = random.randint(1, 250)
    doseUnits = random.choice(["ml", "mg"])
    quanity = random.randint(1, 50)
    medID = random.randint(1, 20000000)
    orderDate = generateDate(admitDate, dischargeDate)
    orderCompletedDate = generateIntervalDate(orderDate)
    med = "\t".join((str(x) for x in [encID, medOrderID, medName, dose, doseUnits, quanity, medID, orderDate, orderCompletedDate])) + "\n"
    q.put(med)
    return med

def medoutputworker(q, outfile):
    """This function takes a queue and a file location. It writes the objects in the queue out to the file specified. If
    the object is "DONE" then the file is closed.
    :param q: The queue to get the objects from
    :param outfile: The location of the file to write the objects
    :return: no returned value
    """
    f = open(outfile, 'wb')
    while True:
        med = q.get()
        if med == "DONE":
            break
        f.write(med)
        f.flush()
    f.close()


def labworker(q, encID, labID, admitDate, dischDate):
    """This function generates a single laboratory order and adds the order the specified queue. The function randomly
    selects a lab name, description, results, results units, accession ID, order date and order completed date. The
    function returns the complete laboratory order
    :param q: the queue the complete order is added to
    :param encID: the encounter id the laboratory order is associated with
    :param labID: the lab order id
    :param admitDate: the encounter admission date
    :param dischDate: the encounter discharge date
    :return: the complete lab order
    """
    LabName = generateLabName()
    LabDescript = LabName + " " + LabName
    LabResultNum = round(random.gauss(2, 1), 2)
    LabResultsUnits = random.choice(["mg/dL", "ppm", "percent"])
    accessionID = random.randint(100000, 1000000)
    labOrderDate = generateDate(admitDate, dischDate)
    LabCompleteDate = generateIntervalDate(labOrderDate)
    lab = "\t".join((str(x) for x in [encID, labID, LabName, LabDescript, LabResultNum, LabResultsUnits, accessionID, labOrderDate, LabCompleteDate])) + "\n"
    q.put(lab)
    return lab


def laboutputworker(q, outfile):
    """This function takes a queue and a file location. It writes the objects in the queue out to the file specified. If
    the object is "DONE" then the file is closed.
    :param q: the queue to get the objects from
    :param outfile: the file to write the objects to
    :return: no return value
    """
    f = open(outfile, 'wb')
    while True:
        lab = q.get()
        if lab == "DONE":
            break
        f.write(lab)
        f.flush()
    f.close()


def procedureTable(numberOfEncounters, procTableFile, admitDict, dischDict):
    """
    Generates the file of procedures. Teh function takes the number of encounters, the file location, the admission
    dictionary and the discharge dictionary. The function generates the procedure id, the procedure name and date.
    :param numberOfEncounters: the number of encounters
    :param procTableFile: the location to write the generated procedures
    :param admitDict: the dictionary of admission dates
    :param dischDict: the dictionary of discharge dates
    :return: no return value
    """
    numProcOrders = tableSize(numberOfEncounters, 2)
    procID = 0
    with open(procTableFile, 'w') as outfile:
        for procedure in xrange(0, numProcOrders):
            encID = random.choice(list(admitDict.keys()))
            procID += 1
            procname = generateProcName()
            procDate = generateDate(admitDict[encID], dischDict[encID])
            proc = "\t".join((str(x) for x in [encID, procID, procname, procDate])) + "\n"
            outfile.write(proc)
    outfile.close()

def procedureworker(q, encID, procedureID, admitDate, dischargeDate):
    procname = generateProcName()
    procDate = generateDate(admitDate, dischargeDate)
    proc = "\t".join((str(x) for x in [encID, procedureID, procname, procDate])) + "\n"
    q.put(proc)
    return proc

def procedureoutputworker(q, outfile):
    f = open(outfile, 'wb')
    while True:
        lab = q.get()
        if lab == "DONE":
            break
        f.write(lab)
        f.flush()
    f.close()


def notesTable(numberOfEncounters, notesTableFile, admitDict, dischDict):
    """
    Generate the notes for the synthetic patient encounters. The function takes the number of encounters, the location
    of the notes file, the dictionary of admission dates, and the dictionary of discharge dates
    :param numberOfEncounters: the number of encounters
    :param notesTableFile: the location to write the synthetic notes
    :param admitDict: admission date
    :param dischDict: discharge date
    :return: no return value
    """
    numberOfNotes = tableSize(numberOfEncounters, 1)
    noteID = 0
    with open(notesTableFile, 'w') as outfile:
        for note in xrange(0, numberOfNotes):
            encID = random.choice(list(admitDict.keys()))
            noteID += 1
            noteText = generateSentences()
            authorID = random.randint(1000, 10000)
            noteDate=generateDate(admitDict[encID], dischDict[encID])
            note = "\t".join((str(x) for x in [encID, noteID, noteText, authorID, noteDate])) + "\n"
            outfile.write(note)
    outfile.close()


def notesworker(q, encID, notesID, admitDate, dischargeDate):
    noteText = generateSentences()
    authorID = random.randint(1000, 10000)
    noteDate=generateDate(admitDate, dischargeDate)
    note = "\t".join((str(x) for x in [encID, notesID, noteText, authorID, noteDate])) + "\n"
    q.put(note)
    return note


def noteoutputworker(q, outfile):
    f = open(outfile, 'wb')
    while True:
        note = q.get()
        if note == "DONE":
            break
        f.write(note)
        f.flush()
    f.close()


def facilityTable(numberOfFacilities, facilityTableFile):
    """
    Generate the synthetic facilities information. This function takes the number of facilities and the location to write
    the facilities data. The function generateds the facility id, the facility name, and city.
    :param numberOfFacilities: the number of facilities to generate
    :param facilityTableFile: the location of the facilities file
    :return: no return value
    """
    facilityID = 0
    with open(facilityTableFile, 'w') as outfile:
        for facility in xrange(0, numberOfFacilities):
            facilityID += 1
            first_name = random.choice(["Saint", "Regional", "Mountain", "Valley", "Sun", "Plains", "Lake", "Mesa", "Arroyo", "Central"])
            last_name = random.choice(["West", "County", "View", "Hillside", "South", "North", "East", "Vista", "Trail", "Canyon", "City"])
            facname = first_name + " " + last_name
            facLocation = random.choice(["Chicago", "Denver", "St Louis", "Pittsburgh", "San Francisco", "New York", "Atlanta", "Dallas", "Los Angeles"])
            fac = "\t".join((str(x) for x in [facilityID, facname, facLocation])) + "\n"
            outfile.write(fac)
    outfile.close()


def patientTable(numberOfpatients, patientTableFile):
    """
    Generate the synthetic patients. The function takes the number of patients to generate and teh location to write the
    synthetic patient information. The function generates a patient id, first name, last name, gender and date of birth
    for all synthetic patients.
    :param numberOfpatients: the number of patients to generate
    :param patientTableFile: the location to write the generated patient data
    :return: no return value
    """
    patientID = 0
    with open(patientTableFile, 'w') as outfile:
        for pt in xrange(0, numberOfpatients):
            patientID += 1
            fname = generateFirstName()
            lname = generateLastName()
            gender = random.choice(["M", "F"])
            dob = generateDOB()
            ptDemo = "\t".join((str(x) for x in [patientID, fname, lname, gender, dob])) + "\n"
            outfile.write(ptDemo)
    outfile.close()


def main():
    numberOfPatients = 10
    numberOfEncounters = 30
    numberOfFacilities = 2
    admissions = {}
    discharges = {}
    # Location of the generated files 
    encounterFile = "/Users/wblack/Desktop/Junk Data Generator/Data Generated/encounters.txt"
    medicationFile = "/Users/wblack/Desktop/Junk Data Generator/Data Generated/medications.txt"
    labsFile = "/Users/wblack/Desktop/Junk Data Generator/Data Generated/labs.txt"
    proceduresFile = "/Users/wblack/Desktop/Junk Data Generator/Data Generated/procedures.txt"
    notesFile = "/Users/wblack/Desktop/Junk Data Generator/Data Generated/notes.txt"
    facilityFile = "/Users/wblack/Desktop/Junk Data Generator/Data Generated/facilities.txt"
    patientFle = "/Users/wblack/Desktop/Junk Data Generator/Data Generated/patients.txt"
    # Generate the junk data
    # This program is a mix of single threaded and multiprocessing. The laboratory and medication data are multiprocessing
    # while all other data is single threaded.  All files are written out as tab delimited text to specified locations.
    print("starting patients " + time.strftime("%c"))
    patientTable(numberOfPatients, patientFle)
    print("patients complete, starting encounters " + time.strftime("%c"))
    encounterTable(numberOfPatients,encounterFile, admissions, discharges)
    print("encounters complete, starting medications " + time.strftime("%c"))
    # Start the multiprocessing by creating a manager to manage the processing pools.
    manager = multiprocessing.Manager()
    q = manager.Queue()  # the queue for the medication orders
    pool = multiprocessing.Pool(multiprocessing.cpu_count())  # The pool used to process medication orders

    watcher = pool.apply_async(medoutputworker, (q, medicationFile,))  # take objects from pool and write out data to file
    # The program first creates the medication order and then writes the order to file. But it is designed to create
    # objects that take from a queue and write to file. Those are started first then the objects that create the med
    # orders and add the orders to the queue.
    numMedOrders = tableSize(numberOfEncounters, 4)
    medjobs = []
    encList = list(admissions.keys())
    # Create the med orders and add orders to the queue
    for med in xrange(1, numMedOrders):
        encID = random.choice(encList)
        medOrderID = random.randint(1, numMedOrders)
        medjob = pool.apply_async(medicationOrder, (q, medOrderID, encID, admissions[encID], discharges[encID],))
        medjobs.append(medjob)
    for job in medjobs:
        job.get()
    # close the medication file. All medication orders are added to queue. When the file writing function finds the
    # "DONE" it will close the file.
    q.put("DONE")

    print("medications complete, starting laboratory" + time.strftime("%c"))

    qlab = manager.Queue()
    watcherlab = pool.apply_async(laboutputworker, (qlab, labsFile,)) # the object that write data to file
    numLabOrders = tableSize(numberOfEncounters, 4) # number of lab orders
    labjobs = [] # the lab order objects
    for lab in xrange(1, numLabOrders): # create the lab order objects and add to queue
        encID = random.choice(encList)
        labOrderID = random.randint(1, numLabOrders)
        labjob = pool.apply_async(labworker, (qlab, labOrderID, encID, admissions[encID], discharges[encID],))
        labjobs.append(labjob)
    for ljob in labjobs:
        ljob.get()

    qlab.put("DONE") # Add the signal to close the lab file


    print("laboratory complete, starting procedures " + time.strftime("%c"))
    qproc = manager.Queue()
    watcherproc = pool.apply_async(procedureoutputworker, (qproc, proceduresFile))
    numProcOrders = tableSize(numberOfEncounters, 2)
    procjobs = []
    for proc in xrange(1,numProcOrders):
        encID = random.choice(encList)
        procID = random.randint(1, numProcOrders)
        procjob = pool.apply_async(procedureworker, (qproc, encID, procID, admissions[encID], discharges[encID],))
        procjobs.append(procjob)
    for pjob in procjobs:
        pjob.get()
    qproc.put("DONE")

    print("procedures complete, starting notes " + time.strftime("%c"))
    qnotes = manager.Queue()
    watchernotes = pool.apply_async(noteoutputworker, (qnotes, notesFile))
    numberOfNotes = tableSize(numberOfEncounters, 1)
    notesjobs = []
    for notes in xrange(1, numberOfNotes):
        encID = random.choice(encList)
        noteID = random.randint(1, numberOfNotes)
        notejob = pool.apply_async(notesworker, (qnotes, encID, noteID, admissions[encID], discharges[encID],))
        notesjobs.append(notejob)
    for njob in notesjobs:
        njob.get()
    qnotes.put("DONE")


    pool.close()  # close the lab processing pool
    pool.join()

    print("notes complete, starting facilities " + time.strftime("%c"))
    facilityTable(numberOfFacilities, facilityFile)
    print("Junk data complete " + time.strftime("%c"))

if __name__ == "__main__":
    main()




