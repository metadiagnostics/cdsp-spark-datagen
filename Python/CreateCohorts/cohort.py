import random
import RandomOrders.DataGenerator

class Cohort(object):
    """A cohort is a group of patients with similar characteristics. In this context those characteristics are
    defined by a risk equation, at this time a logistic regression model. The risk equation uses features that are
    derived from patient data.  This class generates or mimics the patient data from which the features are derived.

    Attributes:
        name: the name of the cohort
        cohortType: type of cohort, i.e. PCI or Suicide risk

    """

    def __init__(self, name, cohortType):
        """Return a Cohort object"""
        self.name = name
        self.cohortType = cohortType

        patients = RandomOrders.DataGenerator.Patient()
        encounters = RandomOrders.DataGenerator.EncounterEvent()
        medication = RandomOrders.DataGenerator.MedEvent()
        lab = RandomOrders.DataGenerator.LabEvent()
        procs = RandomOrders.DataGenerator.ProcedureEvent()
        facilities = RandomOrders.DataGenerator.Facility()
        notes = RandomOrders.DataGenerator.NoteEvent()



class PCI(Cohort):
    """Defining a cohort of patients with PCI at risk for readmission.
    The features of the PCI model are:
        1. being female,
        2. age,
        3. number of comorbid conditions,
        4. length of stay,
        5. severity
    Severity will be defined as the number of arteries blocked
    The generalized linear model with logit link for PCI readmission risk is:
        outcome = -2.4 + 0.2*female + 0.5*comorbid + 0.7*length_stay + 0.2*severity.
    The risk is calculated using this equation. The outcome of which is converted to a probability using a
    Each feature does not have to be "positive" for the feature but the risk of readmission is the probabilty using a
    Bernoulli distribution. The Bernoulli takes the probability and translates that to a binary (zero/one) outcome. It
    says, if we have this risk of outcome, i.e. 75% risk of readmission, what outcome will we see, usually we can expect
    the outcome of readmission, but not always.
    """
    def __init__(self):
        Cohort.__init__()






class Suicide(Cohort):
    """Defining a cohort of patients at risk of Suicide.
    The features of the suicide risk model are:
    the risk factors are male, enlistment age >= 27,
        1. prior suicide attempt
        2. number of outpt visits with suicidal ideation in past 12 months
        3. number of outpt mental health professional visits in last 12 months
        4. number antidepressant prescriptions filled in last 12 months
        5. number of psychiatric hospitalizations/time in service >50th percentile
        6. any prior inpat psychiatric treatment in past 12 months
        7. number of inpt days in past 12 months for major depression
        8. somatoform or disassociative disorder
        9. diagnosis of ptsd or depression
        10. substance misuse (alcohol)
    The generalized linear model with a logit link for suicide risk is:
        outcome = -15 + 0.5*male + 0.1*elist_age27 + 1.2*prior_attempt + 1.8*numOutpt_ideation
        + 1.02*num_Antidepressants + 2*num_psych_hosp + 1.4*prior_inpt_psych + 1.7*num_days_depression
        + 2.0*somatoform_dissoc + 2.4*ptsd_depression + 0.2*substance_alcohol.
    The risk is calculated using this equation. The outcome of which is converted to a probability.probabilty using a
    Bernoulli distribution. he Bernoulli takes the probability and translates that to a binary (zero/one) outcome. It
    says, if we have this risk of outcome, i.e. 75% risk of suicide, what outcome will we see, usually we can expect
    the outcome of suicide, but not always.
    """

    def __init__(self):
        Cohort.__init__()

    def generateAntidepressantOrders(self):
        """This function generates an order for an Antidepresant"""
        antidepressant = Cohort.medication


    def priorAttempt(self):
        """Generate encounter with diagnosis code for suicide attempt"""
        self.