import random
import time
from datetime import datetime, timedelta


class Utilities(object):
    """ This class contains utility and helper functions"""

    def __init__(self, name):
        self.name = name


    def stringToTime(self, stringTime):
        """ Take a string and return a date and time formatted as 12/24/2016 12:45 AM
        :param stringTime: the string date and time
        :return: formatted date time string
        """
        format = '%m/%d/%Y %I:%M %p'
        timeFromString = time.mktime(time.strptime(stringTime, format))
        return timeFromString


    def generateDate(self, startDate, endDate):
        """ Returns a random date between a starting date and ending date. The returned date is in
        the format of 12/24/2016 12:45 AM
        :param startDate: the starting date
        :param endDate: the end date
        :return: a randomly selected date between the start date and end date
        """
        format = '%m/%d/%Y %I:%M %p'
        startTime = self.stringToTime(startDate)
        endTime = self.stringToTime(endDate)
        randDate = startTime + random.random() * (endTime - startTime)
        return time.strftime(format, time.localtime(randDate))


    def generateIntervalDate(self, admitDate):
        """Generate a date of a random interval from the admit date
        :param admitDate: user supplied date to start interval
        :return: a date after the user supplied date
        """
        format = '%m/%d/%Y %I:%M %p'
        intervaldays = random.randint(1, 20)
        intervalhrs = random.randint(1, 12)
        disch = (datetime.strptime(admitDate, format) + timedelta(days=intervaldays) + timedelta(hours=intervalhrs))
        return disch.strftime(format)


    def tableSize(self, startSize, multiplicative):
        """Generate the number of orders in a table. The size of the table is a random number between the start size and
        the start size times the multiplicative.
        :param startSize: The start size is the lowest number of orders
        :param multiplicative: The order of magnitude larger than the start size the table should be
        :return: The size of the table
        """
        return self.startSize + random.randint(self.startSize, self.startSize*multiplicative)