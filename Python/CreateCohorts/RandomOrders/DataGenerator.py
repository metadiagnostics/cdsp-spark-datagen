import multiprocessing
import random
import Utilities
from datetime import datetime, timedelta
import loremipsum


class Counter(object):
    """ This class is the counters that keep track of the order identification numbers, encounter id numbers,
    facility id numbers, and patient id numbers to ensure that each order/patient/facility has a unique
    identification number"""
    medOrderID = 0
    labOrderID = 0
    procOrderID = 0
    encounterID = 0
    patientID = 0
    facilityID = 0
    noteID = 0
    dxID = 0

    def __init__(self):
        self

    def getMedOrderID(self):
        """This function keeps track of the medication order id number.
        The function increases the medication order id counter by 1 and returns the count.
        :return: current value of the medication order id counter"""
        self.medOrderID += 1
        return self.medOrderID

    def getEncID(self):
        """This function keeps track of the encounter number
        This function increases the encounter id count by 1 and returns the new count
        :return: value of the encounter id counter"""
        self.encounterID += 1
        return self.encounterID

    def getPatientId(self):
        """This function keeps track of the patient identification number
        This function increases the patient ID count by 1 and returns the new count
        :return: value of the patient id counter"""
        self.patientID += 1
        return self.patientID

    def getDxOrderID(self):
        """This function keeps track of the diagnosis id number.
        The function increases the diagnoses id counter by 1 and returns the count.
        :return: current value of the diagnosis id counter"""
        self.dxID += 1
        return self.dxID

    def getProcOrderID(self):
        """This function keeps track of the procedure order id number.
        The function increases the procedure order id counter by 1 and returns the count.
        :return: current value of the procedure order id counter"""
        self.procOrderID += 1
        return self.procOrderID

    def getLabOrderID(self):
        """This function keeps track of the laboratory order id number.
        The function increases the laboratory order id counter by 1 and returns the count.
        :return: current value of the laboratory order id counter"""
        self.labOrderID += 1
        return self.labOrderID

    def getNoteID(self):
        """This function keeps track of the note id number.
        The function increases the note id counter by 1 and returns the count.
        :return: current value of the procedure order id counter"""
        self.procOrderID += 1
        return self.procOrderID


class MultiprocessingObj(object):
    """ This class handles the multiprocessing of the data generated. Specifically it handles writing the data out
    to files.
    Each event type (order, encounter, facility etc) is written to a unique tab delimited file"""
    manager = multiprocessing.Manager()
    q = manager.Queue()
    pool = multiprocessing.Pool(multiprocessing.cpu_count())

    def watchers(self, workerName, outfile):
        """This function creates a watcher pool. The watcher or consumer waits for the producer to create an object and add
        that object to a queue. The watcher then processes the objects in the queue
         :param workerName: The name of the worker object
         :param outfile: The location and name of the output file the worker object is to use
        """
        watcher = self.pool.apply_async(workerName, (self.q, outfile))
        return watcher

    def dataGenerator(self):
        # Start the multiprocessing by creating a manager to manage the processing pools.
        manager = multiprocessing.Manager()
        q = manager.Queue()  # the queue for the medication orders
        pool = multiprocessing.Pool(multiprocessing.cpu_count())  # The pool used to process orders

        watcher = pool.apply_async(medoutputworker, (q, medicationFile,))  # take objects from pool and write out data to file


class DataEvent(object):
    """ This class is the generic class for the events generated in the patient data generator"""

    utilities = Utilities("utilities")
    counter = Counter("counter")
    numFacilities = 50

    def __init__(self):

    def createNameDict(self, resoureNames):
        """The function creates a dictionary of medication names. The medication names are in a tab delimited file located in
        the resources directory of this application and are the keys, the value is a randomly generated number. The
         randomly generated number is the medication identification number. The function returns a dictionary.
         :param resoureNames: The file in the resources directory that contains the names used to create the dictionary
        :return: a dictionary of the medication name as the key and a randomly generated number as the value
        """
        infileNames = list(open(resoureNames).read().splitlines())
        randomIDNumbers = random.sample(range(1000000), (len(infileNames) + 1))
        outdictionary = {name: randomIDNumbers(i) for i, name in enumerate(infileNames)}
        return outdictionary

    def createSpecificNameDict(self, resourceFile, dictionary):
        """This function takes a specific resource directory and creates a dictionary of the names in the resource file
        as the key and a random id number as the value.  The names listed in the directory file are also in the general
        resource directory file, i.e. a list of medication names in a specific class (antidepressants) will also be
        included in the general medication names resource file
        :param resourceFile
        :param dictionary: The dictionary that contains the names and identification numbers
        :return dictionary with names as keys and id number as values"""
        resourcelocation = "resources/" + resourceFile + ".txt"
        specificNames = list(open(resourcelocation).read().splitlines())
        outdictionary = {name: dictionary[name] for name in specificNames}
        return outdictionary

    def generateRandomName(self, dictionary):
        """Select a random name from a list of dictionary keys
        :return: A randomly selected medication name
        """
        return random.choice(list(dictionary.keys()))

    def generateRandomEntry(self, dictionary):
        """Select a random key and value from a dictionary"""
        return  random.sample(dictionary.items(), 1)


class MedEvent(DataEvent):
    """ This class generates a medication event"""

    medNameDictionary= {}
    medNameGroupDictionary={}

    def __init__(self):
        DataEvent.__init__()
        # Dictionary of randomly generated medication ids as keys and names as values
        if not MedEvent.medNameDictionary:
            DataEvent.createNameDict("resources/MedNames")

    def addMedGroup(self, medGroup, medName):
        """This function adds a medication to a specific group. The groups are stored as a dictionary where the key is
        the medication group and the value is a list of mednames"""
        if not medGroup in MedEvent.medNameGroupDictionary:
            MedEvent.medNameGroupDictionary[medGroup] = list(medName)

    def getMedGroup(self, medGroup):
        """This function takes the name of the medication group and returns a list of the medications in that group
        :param medGroup: The medication group
        :return list of medications in the group"""
        return MedEvent.medNameGroupDictionary[medGroup]

    def medicationRandomOrder(self, encID, admitDate, dischargeDate):
        """Create a single medication order with a random medication name, medication id number etc.
        The function takes the encounter ID, the medication order id, the admission date, and discharge date from the encounter. The function randomly selects
        1) the medication name, the dose, units, order date and order completed date. The entire generated order is then added
        to a queue for writing out to a file.
        :param encID: the encounter id the medication order is associated with
        :param admitDate: the admission date
        :param dischargeDate: the discharge date
        :return: the medication order as a tab delimited string
        """
        medName = DataEvent.generateRandomName(self, MedEvent.medNameDictionary)
        medOrderID = Counter.getMedOrderID()
        dose = random.randint(1, 250)
        doseUnits = random.choice(["ml", "mg"])
        quanity = random.randint(1, 50)
        medID = MedEvent.medNameDictionary[medName]
        orderDate = DataEvent.utilities.generateDate(admitDate, dischargeDate)
        orderCompletedDate = DataEvent.utilites.generateIntervalDate(orderDate)
        med = "\t".join((str(x) for x in [encID, medOrderID, medName, dose, doseUnits, quanity, medID, orderDate, orderCompletedDate])) + "\n"
        return med

    def medicationSpecificOrder(self, medicationName, encID, startDate, endDate):
        """Create a single medication order for a specific medication.
        The function takes the encounter ID, the medication order ID, the start date and end date of the interval during
        which the order should happen. This interval could be the admit and discharge dates of an encounter.
        The function looks up the corresponding key in the medname dictionary to get the mednameID.
        The key is a randomly generated ID, but using dictionary means that it is the same every time that medication name
        is used.
        :param medicationName: is the name of the specific medication
        :param encID: the encounter id
        :param startDate: the start date of the interval
        :param endDate: the end date of the interval
        :return: the medication order as a tab delimited string
        """
        medOrderID = Counter.getMedOrderID()
        dose = random.randint(1, 250)
        doseUnits = random.choice(["ml", "mg"])
        quanity = random.randint(1, 50)
        medID = MedEvent.medNameDictionary(medicationName)
        orderDate = DataEvent.utilities.generateDate(startDate, endDate)
        orderCompletedDate = DataEvent.utilities.generateIntervalDate(orderDate)
        med = "\t".join((str(x) for x in [encID, medOrderID, medicationName, dose, doseUnits, quanity, medID, orderDate, orderCompletedDate])) + "\n"
        return med

class EncounterEvent(DataEvent):
    """This class generates encounter events. The admission and discharge dates are stored in """

    encounterTypeDictionary={}
    admitDictionary={}
    dischargeDictionary={}

    def __init__(self):
        DataEvent.__init__()
        # Create a dictionary of encounter types from the encounter types resource file
        if not EncounterEvent.encounterTypeDictionary:
            DataEvent.createSpecificNameDict(self, "EncounterTypes", EncounterEvent.encounterTypeDictionary)

    def generateEncType(self):
        """Select a random type of encounter from a list of encounter types. The types of encounters are listed in the
        EncounterType file in /resources.
        :return: A randomly selected encounter type from the encounter type dictionary
        """
        return DataEvent.generateRandomName(self, EncounterEvent.encounterTypeDictionary)

    def generateRandomEncounter(self, patientID):
        """This function generates an encounter. The function takes the patientID, randomly generates the admission
        and discharge dates, randomly selects the encounter type and randomly selects the facility. The function stores
        the admission and discharge dates in the admission dictionary and discharge dictionary accessable to all
        instances of the EncounterEvent class. The function returns the table delimited encounter information as
        a string.

        :param patientID: The patient identification number
        :return: encounter information in tab delimited string
        """
        encID = Counter.getEncID()
        admitDate = DataEvent.utilities.generateDate("1/1/2000 1:00 AM", "12/10/2015 12:59 PM")
        dischargeDate = DataEvent.utilities.generateIntervalDate(admitDate)
        encType = self.generateEncType()
        facilityID = random.randint(1, DataEvent.numFacilities)
        EncounterEvent.admitDictionary[encID] = admitDate
        EncounterEvent.dischargeDictionary[encID] = dischargeDate
        enc = "\t".join((str(x) for x in [patientID, encID, admitDate, dischargeDate, encType, facilityID])) + "\n"
        return enc

    def generateRandomEncounterInterval(self, patientID, intervalEndDate, timediff, encType, facilityID):
        """This function generates an encounter within a given interval. The function takes the end date of the
        interval, the number of days the of the interval, the type of encounter, and the facility id.
        The end of the interval is given in the format 12/24/2016 12:45 AM. The internal start date is calculated to be
        the specified number of days prior to the end, i.e. 365 days or 12/24/2015 12:45 AM. The encounter admission
        and discharge dates are randomly selected between the interval start and end dates. the function returns the
        encounter information as a tab delimited string.
        :param patientID: the patient identification number
        :param intervalEndDate: The end of the interval of time in the format 12/24/2016 12:45 AM
        :param timediff: the number of days in the time interval
        :param encType: the type of encounter
        :param facilityID: the facility identification number
        :return: a encounter information as a tab delimited string
        """
        encID = self.counter.getEncID()
        intervalStartDate = self.utilities.stringToTime(intervalEndDate) - timedelta(days=timediff)
        admitDate = self.utilities.generateDate(intervalStartDate, intervalEndDate)
        dischargeDate = self.utilities.generateIntervalDate(admitDate)
        enc = "\t".join((str(x) for x in [patientID, encID, admitDate, dischargeDate, encType, facilityID]))
        return enc

class Patient(DataEvent):
    """This class generates the patients"""

    firstNameDictionary={}
    lastNameDictionary={}

    def __init__(self):
        DataEvent.__init__()
        if not Patient.firstNameDictionary:
            DataEvent.createSpecificNameDict("FirstNames", Patient.firstNameDictionary)
        if not Patient.lastNameDictionary:
            DataEvent.createSpecificNameDict("LastNames", Patient.lastNameDictionary)

    def generateDOB(self):
        """Generate a date of birth for synthetic patients, the start date is 1/1/1935 to 12/28/2000
        :return: A generated date of birth
        """
        dateformat = '%m/%d/%Y'
        mon = random.randint(1, 12)
        d = random.randint(1, 28)
        yr = random.randint(1935, 2000)
        dob = datetime(year=yr, month=mon, day=d)
        dob_f = dob.strftime(dateformat)
        return dob_f

    def generateFirstName(self):
        """Select a random first name from a list of popular first names
        :return: A randomly selected first name
        """
        return DataEvent.generateRandomName(self, Patient.firstNameDictionary)

    def generateLastName(self):
        """Select a random last name from the last name dictionary.
        :return A randomly selected last name"""
        return DataEvent.generateRandomName(self, Patient.lastNameDictionary)

    def generatePatient(self):
        """This function generates a patient. The function calls the counter class for the next  a patient id,
        The function also randomly selects a first name, last name, gender and date of birth for the synthetic patient.
        The function returns a tab delimited string of patient information
        :return: The function returns a tab delimited string of patient id, first and last name, gender, and dob
        """
        patientID = Counter.getPatientId()
        fname = self.generateFirstName()
        lname = self.generateLastName()
        gender = random.choice(["M", "F"])
        dob = self.generateDOB()
        pt = "\t".join((str(x) for x in [patientID, fname, lname, gender, dob])) + "\n"
        return pt


class DiagnosisEvent(DataEvent):
    """This class generates diagnosis codes"""

    dxNamesDictionary={}
    dxSequenceNumDictionary={}

    def __init__(self):
        DataEvent.__init__()
        if not DiagnosisEvent.dxNamesDictionary:
            infileNames = list(open("/resources/Diagnoses").read().splitlines())
            DiagnosisEvent.dxNamesDictionary = dict( (dx, description) for dx, description in (dxcodes.split("/t") for dxcodes in infileNames) )


    def generateSequenceNum(self, encID):
        """This function generates the sequence number. Each encounter can have more than one diagnosis code. Each code
        is given a number representing its priority, i.e. sequence number of 1 is the main cause for the admission. This
        function takes the encounter identification number, looks up the current sequence number in the sequence number
        dictionary, increments the count, and returns the new sequence number.
        :param encID: The encounter identification number
        :return: sequence number
        """
        currentSequence = 1
        if encID in DiagnosisEvent.dxSequenceNumDictionary:
            currentSequence = DiagnosisEvent.dxNamesDictionary[encID]
            currentSequence =+ 1
        DiagnosisEvent.dxSequenceNumDictionary[encID] = currentSequence
        return currentSequence

    def generateRandomDx(self, encID):
        """This function generates a random diagnosis code for an encounter. This function takes the encounterID, and
        returns a tab delimited list of dx information
        :param encID: The encounter identification number
        :return: tab delimited string of diagnosis information
        """
        dxID = Counter.getDxOrderID()
        dxCode = DataEvent.generateRandomEntry(self, DiagnosisEvent.dxNamesDictionary)
        sequenceNum = self.generateSequenceNum(encID)
        dx = "\t".join(str(x) for x in [encID, dxID, dxCode[0], dxCode[1], sequenceNum]) + "\n"
        return dx

    def generateSpecificDx(self, encID, dxCode):
        """This function generates a specific diagnosis code for an encounter. This function takes the encounterID,
        and a specific diagnosis code and returns a tab delimited list of dx information
        :param encID: The encounter identification number
        :param dxCode: The diagnosis code
        :return: tab delimited string of diagnosis information
        """
        dxID = Counter.getDxOrderID()
        dxDescription = DiagnosisEvent.dxNamesDictionary[dxCode]
        sequenceNum = self.generateSequenceNum(encID)
        dx = "\t".join(str(x) for x in [encID, dxID, dxCode, dxDescription, sequenceNum]) + "\n"
        return dx


class ProcedureEvent(DataEvent):
    """This class generates procedures"""
    
    procNamesDictionary={}

    def __init__(self):
        DataEvent.__init__()
        if not ProcedureEvent.procNamesDictionary:
            DataEvent.createNameDict(self, "resources/Procedures")


    def generateRandomProcedure(self, encID, admitDate, dischargeDate):
        """This function generates a random procedure for an encounter. This function takes an encounterID,
        the encounter admission and discharge dates, generates a random procedure datetime between
        the encounter admission and discharge dates and returns a tab delimited string of procedure information
        :param encID: The encounter identification number
        :return: tab delimited string of procedure information
        """
        procname = ProcedureEvent.generateRandomName(self, ProcedureEvent.procNamesDictionary)
        procDate = DataEvent.utilities.generateDate(admitDate, dischargeDate)
        procedureID = Counter.getProcOrderID()
        proc = "\t".join((str(x) for x in [encID, procedureID, procname, procDate])) + "\n"
        return proc

class LabEvent(DataEvent):
    """This class generates labortory tests and results"""

    labNamesDictionary={}

    def __init__(self):
        DataEvent.__init__()
        if not LabEvent.labNamesDictionary:
            DataEvent.createNameDict(self, "resources/LabNames")
        
    def generateRandomLabOrder(self, encID, admitDate, dischargeDate):
        """This function generates a single laboratory order. The function takes the encounter id, the admission date,
         and the discharge date and randomly selects a lab name, description, results, results units, accession ID, 
         order date and order completed date. The function returns the complete laboratory order
        :param encID: the encounter id the laboratory order is associated with
        :param admitDate: the encounter admission date
        :param dischargeDate: the encounter discharge date
        :return: the complete lab order
        """
        labName = DataEvent.generateRandomName(self, LabEvent.labNamesDictionary)
        labID = Counter.getLabOrderID()
        labDescript = labName + " " + labName
        labResultNum = round(random.gauss(2, 1), 2)
        labResultsUnits = random.choice(["mg/dL", "ppm", "percent"])
        accessionID = random.randint(100000, 1000000)
        labOrderDate = DataEvent.utilities.generateDate(admitDate, dischargeDate)
        labCompleteDate = DataEvent.utilities.generateIntervalDate(labOrderDate)
        lab = "\t".join((str(x) for x in [encID, labID, labName, labDescript, labResultNum, labResultsUnits, accessionID, labOrderDate, labCompleteDate])) + "\n"
        return lab
    
    def generateSpecificLabOrder(self, encID, admitDate, dischargeDate, labName):
        """This function generates a single laboratory order. The function randomly
        generates results, results units, accession ID, order date and order completed date. The
        function returns the complete laboratory order
        :param encID: the encounter id the laboratory order is associated with
        :param admitDate: the encounter admission date
        :param dischargeDate: the encounter discharge date
        :param labName: The name of the lab test
        :return: the complete lab order
        """
        labID = Counter.getLabOrderID()
        labDescript = labName + " " + labName
        labResultNum = round(random.gauss(2, 1), 2)
        labResultsUnits = random.choice(["mg/dL", "ppm", "percent"])
        accessionID = random.randint(100000, 1000000)
        labOrderDate = DataEvent.utilities.generateDate(admitDate, dischargeDate)
        labCompleteDate = DataEvent.utilities.generateIntervalDate(labOrderDate)
        lab = "\t".join((str(x) for x in [encID, labID, labName, labDescript, labResultNum, labResultsUnits, accessionID, labOrderDate, labCompleteDate])) + "\n"
        return lab


class NoteEvent(DataEvent):
    """This class generates notes for an encounter"""

    def __init__(self):
        DataEvent.__init__()


    def generateSentences(self):
        """This function generates a set of sentences using lorum ipsum text.
        :return: A random number of lorum ipsum sentences
        """
        outStringList = loremipsum.get_sentences(random.randint(2, 6), True)
        outString = " ".join(outStringList)
        return outString


    def generateNote(self, encID, admitDate, dischargeDate):
        """This function generate the note for an encounters. The function takes the encounter identification, the
        admission date, and the discharge date and returns a tab delimited list of note information.
        :param encID: The encounter identification number
        :param admitDate: The admission date
        :param dischargeDate: The discharge date
        :return: tab delimited string of note information
        """
        noteID = Counter.getNoteID()
        noteText =self.generateSentences()
        authorID = random.randint(1000, 10000)
        noteDate = DataEvent.utilities.generateDate(admitDate, dischargeDate)
        note = "\t".join((str(x) for x in [encID, noteID, noteText, authorID, noteDate])) + "\n"
        return note


class Facility(DataEvent):
    """This class generates the facility information"""

    facilitiesNamesDictionary={}

    def __init__(self):
        DataEvent.__init__()
        if not Facility.facilitiesNamesDictionary:
            Facility.generateFacilities()


    def generateFacilities(self):
        """This function generates all the facilities. This function takes no parameters and returns no value.
        The number of facilities is set by the DataEvent class. This function generates the facility id, the facility name,
        and city. Then it adds the facility information to the facilitesNamesDictionary. The key is the facilityID, and the
        value is a tab delimited string of the facility information. The information includes the facility id, for ease of
        later processing.
        :return: no return value
        """
        facilityID = 0
        for facility in xrange(0, DataEvent.numFacilities):
            facilityID += 1
            first_name = random.choice(["Saint", "Regional", "Mountain", "Valley", "Sun", "Plains", "Lake", "Mesa", "Arroyo", "Central"])
            last_name = random.choice(["West", "County", "View", "Hillside", "South", "North", "East", "Vista", "Trail", "Canyon", "City"])
            facname = first_name + " " + last_name
            facLocation = random.choice(["Chicago", "Denver", "St Louis", "Pittsburgh", "San Francisco", "New York", "Atlanta", "Dallas", "Los Angeles"])
            fac = "\t".join((str(x) for x in [facilityID, facname, facLocation])) + "\n"
            Facility.facilitiesNamesDictionary[facilityID]=fac





