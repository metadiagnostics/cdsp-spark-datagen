"""The LPI cohort creator generates patient data with disease cohorts for the purpose of testing feature selection and risk
models."""

# Location of the generated files
encounterFile = "/Users/wblack/Desktop/Junk Data Generator/Data Generated/encounters.txt"
medicationFile = "/Users/wblack/Desktop/Junk Data Generator/Data Generated/medications.txt"
labsFile = "/Users/wblack/Desktop/Junk Data Generator/Data Generated/labs.txt"
proceduresFile = "/Users/wblack/Desktop/Junk Data Generator/Data Generated/procedures.txt"
notesFile = "/Users/wblack/Desktop/Junk Data Generator/Data Generated/notes.txt"
facilityFile = "/Users/wblack/Desktop/Junk Data Generator/Data Generated/facilities.txt"
patientFle = "/Users/wblack/Desktop/Junk Data Generator/Data Generated/patients.txt"

# Data generation parameters
numberOfPatients = 10
numberOfEncounters = 30
numberOfFacilities = 2
admissions = {}
discharges = {}


def main():

    if __name__ == '__main__':
        main()