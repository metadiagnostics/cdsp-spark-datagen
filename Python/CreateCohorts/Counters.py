class Counter(object):
    """ This class is the counters that keep track of the order identification numbers, encounter id numbers,
     facility id numbers, and patient id numbers to ensure that each order/patient/facility has a unique
    identification number"""
    medOrderID = 0
    labOrderID = 0
    procOrderID = 0
    encounterID = 0
    patientID = 0
    facilityID = 0
    noteID = 0

    def __init__(self):
        self

    def getMedOrderID(self):
        """This function keeps track of the medication order id number.
        The function increases the medication order id counter by 1 and returns the count.
        :return: current value of the medication order id counter"""
        self.medOrderID += 1
        return self.medOrderID

    def getEncID(self):
        """This function keeps track of the encounter number
        This function increases the encounter id count by 1 and returns the new count
        :return: value of the encounter id counter"""
        self.encounterID += 1
        return self.encounterID
