# This file was begun to generate a tab delimited file of fake patient addresses.  The data is not meant to look real
# in any way. This file is a request from the Tech Lead for CIS in order to exercise an API.
# The columns needed are address1, address2, city, region, state, zip code
# This file as begun on 5/13/2016
# Author: Wynona Black, PhD
#
# Note: Replace <PROJDIR> with your Project Directory
#

import random
import sys

outFile = "<PROJDIR>/Data/Junk_Data_Generator/patientAddress.txt"
if len(sys.argv) > 1:
    outFile = sys.argv[1]
    print("OUTPUT FILE SET TO: {}\n".format(outFile))

def getAddress(patientID):
    """This function takes an ID and returns a string of the address1, address2, city, region, state, and zipcode
    :param patientID: the identifier
    :return: a tab delimted string of random patientID, address1, address2, city, region, state, and zipcode values
    """
    address_1 = str(random.randint(1000, 100000)) + " " + random.choice(["Main", "Lakewood", "Ceder", "Bent", "Willow", "Canyon", "Palace", "Park", "Oak", "Maple"]) + " " + random.choice(["Ave", "St", "Ln", "Blvd", "Pl","Dr"])
    address_2 = random.choice(["number", "box"]) + " " + str(random.randint(1, 100000)) if random.randint(1,10) < 2 else ""
    city = random.choice(["SunnyDale", "Gotham", "Hill", "Valley", "Centersville", "Small Town", "Big City", "Rural", "Mills", "Constitution", "Green", "Vale"])
    region = random.choice(["South", "West", "Midwest", "Northeast", "Northwest", "Southwest", "Southeast"])
    state = random.choice(["AL", "AK", "AZ", "AR", "CA", "CO", "CT", "DE", "FL", "GA", "HI", "ID", "IL", "IN", "IA", "KS", "KY", "LA", "ME", "MD", "MA", "MI", "MN", "MS", "MO", "MT", "NE", "NV", "NH", "NJ", "NM", "NY", "NC", "ND", "OH", "OK", "OR", "PA", "RI", "SC", "SD", "TN", "TX", "UT", "VT", "VA", "WA", "WV"])
    zipcode = random.choice([10000, 99999])
    completeData = "\t".join((str(x) for x in [patientID, address_1,address_2, city, region, state, zipcode])) + "\n"
    return completeData


def main():
    numOfPatients = 1000000
    with open(outFile, "w") as outfile:
        headers = "\t".join((str(x) for x in ["patientID", "address_1", "address_2", "city", "region", "state", "zipcode"])) + "\n"
        outfile.write(headers)
        for i in xrange(1, numOfPatients+1):
            outline = getAddress(i)
            outfile.write(outline)

if __name__ == "__main__":
    main()
