#
# Note: Replace the <PROJDIR> references with your own Project Install Dir
#

import fileinput

# The headers
dxheaders = ["EncounterID", "DiagnosisID", "DiagnosisCode", "DiagnosisDescription", "Sequence"]
encheaders = ["PatientID", "EncounterID", "AdmitDate", "DischargeDate", "EncounterType", "FacilityID"]
facheaders = ["FacilityID", "FacilityName", "FacilityLocation"]
labheaders = ["EncounterID", "LabOrderID", "LabName", "LabDescription", "LabValue", "LabValueUnits", "AccessionID", "LabOrderDate", "LabCompletedDate"]
medheaders = ["EncounterID", "MedOrderID", "MedicationName", "MedicationDose", "MedicationDoseUnits", "MedicationQuantity", "MedicationID", "MedOrderDate", "MedOrderCompleteDate" ]
noteheader = ["EncounterID", "NoteID", "NoteText", "AuthorID", "NoteDate"]
ptheaders = ["PatientID", "FirstName", "LastName", "Gender", "DOB"]
procheaders = ["EncounterID", "ProcedureOrderID", "ProcedureName", "ProcedureDate"]

# The files
dxFile = '<PROJDIR>/DataGenerated/diagnoses.txt'
encFile = '<PROJDIR>/DataGenerated/encounters.txt'
facFile = '<PROJDIR>/DataGenerated/facilities.txt'
labFile = '<PROJDIR>/DataGenerated/labs.txt'
medFile = '<PROJDIR>/DataGenerated/medications.txt'
noteFile = '<PROJDIR>/DataGenerated/notes.txt'
ptFile = '<PROJDIR>/DataGenerated/patients.txt'
procFile = '<PROJDIR>/DataGenerated/procedures.txt'



def addHeaders(headers, file):
    """ Add a header to a file. The file is edited inplace
    :param headers: the headers to be added in a list
    :param file: the file to add headers
    :return: no return value
    """
    for line in fileinput.input(file, inplace=True):
        if fileinput.isfirstline():
            print("\t".join(headers))
        print line,


def main():
    addHeaders(ptheaders, ptFile)
    addHeaders(procheaders, procFile)
    addHeaders(noteheader, noteFile)
    addHeaders(medheaders, medFile)
    addHeaders(labheaders, labFile)
    addHeaders(facheaders, facFile)
    addHeaders(encheaders,encFile)
    addHeaders(dxheaders, dxFile)


if __name__ == "__main__":
    main()
