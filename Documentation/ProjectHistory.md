# Project History

TODO: Complete history and add future directions

The DataGen project began as an effort to build patient level demographic data that met certain statistical criteria.  The data were used for various investigations and research.  

In late 2015, the code was reorganized and extended, adding utilities for producing encounter level data.
